## Get department

Get department data like current academic year and period etc

    GET https://api.epicur.auth.gr/api/LocalDepartments?$filter=abbreviation eq 'EPICUR'

with response

        {
            "value": [
                {
                    "id": "15001",
                    "registrationPeriodStart": "2021-03-23T22:00:00.000Z",
                    "registrationPeriodEnd": "2021-04-14T21:00:00.000Z",
                    "organization": 151,
                    "name": "EPICUR",
                    "abbreviation": "EPICUR",
                    "city": null,
                    "address": null,
                    "postalCode": null,
                    "country": null,
                    "alternativeCode": "15001",
                    "currentYear": {
                        "id": 2020,
                        "identifier": null,
                        "additionalType": "AcademicYear",
                        "alternateName": "2020-2021",
                        "description": "Academic Year 2020-2021",
                        "image": null,
                        "name": "2020-2021",
                        "url": null,
                        "dateCreated": null,
                        "dateModified": null,
                        "createdBy": null,
                        "modifiedBy": null
                    },
                    "currentPeriod": {
                        "id": 2,
                        "identifier": 2,
                        "additionalType": "AcademicPeriod",
                        "alternateName": "summer",
                        "description": null,
                        "image": null,
                        "name": "Spring",
                        "url": null,
                        "dateCreated": "2021-01-23T18:44:20.980Z",
                        "dateModified": "2021-01-23T18:44:20.980Z",
                        "createdBy": 6,
                        "modifiedBy": 6
                    },
                    "facultyName": null,
                    "facultyName2": null,
                    "departmentName2": null,
                    "name2": null,
                    "phone1": null,
                    "phone2": null,
                    "studyLevel": {
                        "id": 1,
                        "identifier": 1,
                        "additionalType": "StudyLevel",
                        "alternateName": "undergraduate",
                        "description": null,
                        "image": null,
                        "name": "Undergraduate",
                        "url": null,
                        "dateCreated": "2021-01-23T18:44:20.762Z",
                        "dateModified": "2021-01-23T18:44:20.765Z",
                        "createdBy": 6,
                        "modifiedBy": 6
                    },
                    "email": null,
                    "url": "https://epicur.education/",
                    "localDepartment": false,
                    "notes": "EPICUR was selected in June 2019 to pilot a new way of intensifying collaboration among Higher Education institutions through the creation of a European University.",
                    "dateModified": "2021-03-25T05:51:24.321Z",
                    "departmentConfiguration": null,
                    "locale": null
                }
            ]
        }

## Get active study programs

Get a list of active study program by querying `StudyPrograms` entity set:

    GET https://api.epicur.auth.gr/api/StudyPrograms?$expand=info,specialties,studyLevel&$filter=isActive%20eq%20true

with response

    {
        "value": [
            {
                "id": 8,
                "department": "15001",
                "studyLevel": {
                    "id": 1,
                    "identifier": 1,
                    "additionalType": "StudyLevel",
                    "alternateName": "undergraduate",
                    "description": null,
                    "image": null,
                    "name": "Undergraduate",
                    "url": null,
                    "dateCreated": "2021-01-23T18:44:20.762Z",
                    "dateModified": "2021-01-23T18:44:20.765Z",
                    "createdBy": 6,
                    "modifiedBy": 6
                },
                "isActive": true,
                "name": "Language and Culture",
                "abbreviation": "LANG",
                "printName": "Language and Culture",
                "degreeDescription": "Language and Culture",
                "gradeScale": 3,
                "decimalDigits": 2,
                "hasFees": false,
                "semesters": 1,
                "specialtySelectionSemester": 1,
                "specialties": [
                    {
                        "id": "509C3F51-45F3-4728-8CE0-DE1991178F58",
                        "studyProgram": 8,
                        "specialty": -1,
                        "name": "CORE",
                        "abbreviation": "C"
                    }
                ],
                "info": {
                    "id": 4,
                    "studyProgram": 8,
                    "studyType": null,
                    "studyTitleType": 2,
                    "notes": "Work Package 2 addresses two core European values: multilingualism, and inclusiveness. Both revolve around the role language has to play in a Europe for the future; the former as its most characteristic strength, the latter as its most important precondition.\nIn working towards a European Education Area by 2025, EPICUR addresses the Commission’s aspiration that new generations of Europeans will speak at least two languages in addition to their own.",
                    "officialDuration": null,
                    "accessRequirements": null,
                    "modeOfStudy": null,
                    "degreeRequirements": null,
                    "accessToFurther": null,
                    "professionalStatus": null,
                    "gradingScheme": null,
                    "otherInfo": null,
                    "furtherInfo": null,
                    "languageOfInstruction": null,
                    "degreeDescription": null,
                    "requiredInternship": null
                }
            }
        ]
    }

## Get courses of a study program

Get a list of study program courses by querying `SpecializationCourses` entity set:

    GET https://api.epicur.auth.gr/api/SpecializationCourses?$expand=specialization,studyProgramCourse($expand=course($expand=courseArea,courseDescription,provider))&$filter=studyProgramCourse/studyProgram/abbreviation eq 'LANG'

or

    GET https://api.epicur.auth.gr/api/SpecializationCourses?$expand=specialization,studyProgramCourse($expand=course($expand=courseArea,courseDescription,provider))&$filter=studyProgramCourse/studyProgram eq 8

with response:

    {
        "value": [
            {
                "id": "027C9E07-4D18-45BB-9585-F78352D71DD6",
                "studyProgramCourse": {
                    "id": 41,
                    "course": {
                        "id": "AMU800",
                        "displayCode": "AMU800",
                        "department": "15001",
                        "courseArea": {
                            "id": 1,
                            "abbreviation": null,
                            "department": "15001",
                            "identifier": 0,
                            "additionalType": "CourseArea",
                            "alternateName": "SLA",
                            "description": "",
                            "image": null,
                            "name": "Slavonic",
                            "url": null,
                            "dateCreated": "2021-01-29T07:50:22.268Z",
                            "dateModified": "2021-01-29T07:50:22.271Z",
                            "createdBy": 6,
                            "modifiedBy": 6
                        },
                        "name": "Introduction to the Balkan Studies. Linguistic Point of View",
                        "subtitle": null,
                        "isEnabled": true,
                        "isShared": true,
                        "gradeScale": 3,
                        "instructor": null,
                        "isCalculatedInScholarship": false,
                        "units": 3,
                        "courseUrl": "https://learn.epicur.education/",
                        "notes": "The course is devoted to the theory of the Balkan Sprachbund (Balkan language area) with special attention to the latest trends in the Balkan studies. The aim is to introduce this theory to the students and to show its problems and complexity. The students will discuss different approaches to the definition of the Balkan and",
                        "replacedByCourse": null,
                        "replacedCourse": null,
                        "maxNumberOfRemarking": null,
                        "parentCourse": null,
                        "coursePartPercent": null,
                        "calculatedCoursePart": false,
                        "courseStructureType": 1,
                        "courseSector": null,
                        "courseCategory": null,
                        "ects": 3,
                        "isLocal": false,
                        "dateModified": "2021-06-03T14:35:33.744Z",
                        "calculatedInRegistration": false,
                        "provider": {
                            "id": 159,
                            "name": "Adam Mickiewicz University in Poznań",
                            "alternateName": "AMU",
                            "identifier": "",
                            "url": "http://international.amu.edu.pl/",
                            "local": false,
                            "contactPerson1": null,
                            "contactPerson2": null,
                            "notes": "",
                            "instituteType": 1,
                            "partner": true,
                            "image": null
                        },
                        "identifier": "183",
                        "courseDescription": {
                            "id": 1,
                            "course": "AMU800",
                            "syllabus": "<p>The course is devoted to the theory of the Balkan Sprachbund (Balkan language area) with special attention to the latest trends in the Balkan studies. The aim is to introduce this theory to the students and to show its problems and complexity. The students will discuss different approaches to the definition of the Balkan and</p>",
                            "additionalInformation": null,
                            "learningOutcome": null,
                            "generalCompetencies": null,
                            "bibliography": null,
                            "languageOfInstruction": null,
                            "focusLanguage": null,
                            "dateCreated": "2021-06-03T14:35:06.004Z",
                            "dateModified": "2021-06-03T14:35:33.765Z",
                            "createdBy": 6,
                            "modifiedBy": 6,
                            "additionalUrl": null
                        }
                    },
                    "studyProgram": 8,
                    "programGroup": null,
                    "programGroupFactor": null,
                    "sortIndex": null,
                    "identifier": "828A64FB-11B2-4B93-BBD5-C16B653068EE",
                    "dateModified": "2021-02-10T16:51:51.837Z"
                },
                specialization": {
                    "id": "509C3F51-45F3-4728-8CE0-DE1991178F58",
                    "studyProgram": 8,
                    "specialty": -1,
                    "name": "CORE",
                    "abbreviation": "C"
                }
                "specializationIndex": -1,
                "semester": 1,
                "coefficient": 1,
                "units": 3,
                "ects": 3,
                "courseType": 1,
                "sortIndex": null,
                "dateModified": "2021-02-10T16:51:51.774Z"
            }
        ]
    }    

## Get course classes

Get a collection of course classes by querying `CourseClasses` entity set:

    GET https://api.epicur.auth.gr/api/CourseClasses?$filter=course/studyProgramCourses/studyProgram%20eq%208%20and%20year%20eq%202020%20and%20period%20eq%202&$expand=course($expand=provider,courseArea,courseDescription)

