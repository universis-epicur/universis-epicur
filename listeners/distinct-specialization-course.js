/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
function beforeExecute(event, callback) {
    /**
     * @type {import('@themost/query').QueryExpression}
     */
    const query = event.emitter && event.emitter.query;
    if (query == null) {
        return callback();
    }
    if (Object.prototype.hasOwnProperty.call(query, '$select')) {
        query.distinct();
    }
    return callback();
}

module.exports = {
    beforeExecute
}