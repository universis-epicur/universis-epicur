const { DataNotFoundError, HttpError } = require('@themost/common');
const { DataObjectState, DataEventArgs } = require('@themost/data');
/**
 * @param {DataEventArgs} event
 */
async function beforeSaveAsync(event) {
    if (event.state === DataObjectState.Insert) {
        const { context } = event.model;
        const { company } = event.target;
        if (company) {
            // find company by id
            const {id, name} = company;
            if (id != null || name != null) {
                const found = await context.model('Company').where((x, id, name) => {
                    return x.id === id || x.name === name;
                }, id, name).silent().getItem();
                if (found) {
                    Object.assign(event.target, {
                        company: found
                    });
                    return;
                }
                if (id) {
                    throw new HttpError(409, 'The specified company/organization cannot be found. It may have been delete or its identifier has not been specified correctly.');
                }
            }
            // otherwise, try to insert company
            await context.model('Company').insert(company);
            // assign inserted company
            Object.assign(event.target, {
                company
            });
        }
    }
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
function beforeSave(event, callback) {
    return beforeSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}

module.exports = {
    beforeSave
}