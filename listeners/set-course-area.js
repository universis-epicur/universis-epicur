const { DataObjectJunction, HasParentJunction, DataObjectState } = require('@themost/data');
/**
 * @param {import('@themost/data').DataEventArgs} event
 * @param {Function} callback
 */
function beforeSave(event, callback) {
    const { target } = event;
    if (Object.prototype.hasOwnProperty.call(target, 'courseAreas') && Array.isArray(target.courseAreas)) {
        const { courseAreas } = target;
        // get first course area
        const [courseArea] = courseAreas.filter((x) => x.$state !== DataObjectState.Delete);
        // set course area
        target.courseArea = courseArea;
    }
    return callback();
}

module.exports = {
    beforeSave
};