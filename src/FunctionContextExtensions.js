import { FunctionContext } from '@themost/data';
import _ from 'lodash';
import moment from "moment";
import { Args, DataNotFoundError, DataError } from '@themost/common';

Object.assign(FunctionContext.prototype, {
    /**
     * @this FunctionContext
     * Returns id for new courseClass based on courseClassFormat  (e.g. {course-year-period}, {00000}).
     * @returns {Promise}
     */
    newCourseClass(target) {
        const self = this;
        return new Promise((resolve, reject) => {
            const context = self.model.context;
            context.db.selectIdentity('CourseClassBase', 'id', (err, res) => {
                if (err) {
                    return reject(err);
                }
                if (target['course'] && target['year'] && target['period']) {
                    let identifier = null;
                    let courseClassFormat = 'I';
                    // get courseClassFormat from settings
                    let universisConfiguration = context.getApplication().getConfiguration().getSourceAt('settings/universis');
                    if (typeof universisConfiguration !== 'undefined') {
                        courseClassFormat = universisConfiguration.courseClassFormat || 'I';
                    }
                    const course = _.isObject(target['course']) ? target['course'].id : target['course'];
                    const year = _.isObject(target['year']) ? target['year'].id : target['year'];
                    const period = _.isObject(target['period']) ? target['period'].id : target['period'];
                    // calculate new id
                    identifier = courseClassFormat;
                    let format = courseClassFormat.split(';');
                    for (let i = 0; i < format.length; i++) {
                        let formatElement = format[i];
                        if (formatElement.startsWith('I')) {
                            // index with leading zeros
                            identifier = identifier.replace(formatElement, this.zeroPad((res + 1), formatElement.length - 1));
                        }
                        if (formatElement.startsWith('T')) {
                            // text
                            identifier = identifier.replace(formatElement, formatElement.substr(1));
                        }
                        if (formatElement.startsWith('C')) {
                            // text
                            identifier = identifier.replace(formatElement, course);
                        }
                        if (formatElement.startsWith('Y')) {
                            // year
                            let today = new Date();
                            today.setFullYear(year, 1, 1, 0);
                            identifier = identifier.replace(formatElement, moment(today).format(formatElement));
                        }
                        if (formatElement.startsWith('P')) {
                            // period
                            identifier = identifier = identifier.replace(formatElement, period);
                        }
                    }
                    identifier = identifier.replace(/;/g, '');
                    return resolve(identifier);
                } else {
                    return resolve();
                }
            });
        })
    },
    zeroPad(num, places) {
        let zero = places - num.toString().length + 1;
        return Array(+(zero > 0 && zero)).join("0") + num;
    },
    async newStudentIdentifier(target) {

        const department = await this.context.model('LocalDepartment').where('id').equal(target.department)
            .select('id', 'abbreviation', 'studentIdentifierIndex', 'studentIdentifierFormat', 'currentYear/id as currentYear', 'currentPeriod/id as currentPeriod')
            .cache(false)
            .getItem()
        Args.check(department != null, new DataError('E_DEPARTMENT', 'The specified department cannot be found or is inaccessible', null, 'LocalDepartment'));
        if (department.studentIdentifierIndex == null) {
            department.studentIdentifierIndex = 1;
        } else {
            department.studentIdentifierIndex += 1;
        }
        // save new index
        await this.context.model('LocalDepartment').save({
            id: department.id,
            studentIdentifierIndex: department.studentIdentifierIndex
        });
        const studentIdentifierFormat = department.studentIdentifierFormat || 'I000000';
        let formatElements = studentIdentifierFormat.split(';');
        return formatElements.map((formatElement) => {
            // format static text
            if (formatElement.startsWith('T')) {
                return formatElement.substr(1);
            }
            // format year e.g. YYYY or YY etc
            if (formatElement.startsWith('Y')) {
                return moment(department.currentYear, 1, 1).format(formatElement);
            }
            if (formatElement.startsWith('I')) {
                return this.zeroPad(department.studentIdentifierIndex, formatElement.length - 1);
            }
            if (formatElement.startsWith('P')) {
                return this.zeroPad(department.currentPeriod, formatElement.length - 1);
            }
        }).join('');

    }

});