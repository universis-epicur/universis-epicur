import { ApplicationService } from "@themost/common";
import { ModelClassLoaderStrategy, SchemaLoaderStrategy, EdmMapping } from "@themost/data";
import { inc, coerce} from 'semver';

class UserExtensions {
    @EdmMapping.func('validate', 'Object')
    async validate() {
        const service = this.context.getApplication().getService(function EpicurService() { });
        if (service == null) {
            throw new Error('Invalid application configuration Epicur service cannot be found.')
        }
        // get user name
        const name = await this.property('name').value();
        return service.validateUser(this.context, name);
    }

    @EdmMapping.func('profile', 'Object')
    async getProfile() {
        const service = this.context.getApplication().getService(function OAuth2ClientService() { });
        if (service == null) {
            throw new Error('Invalid application configuration. Authorization service cannot be found.')
        }
        return service.getProfile(this.context, this.context.user.authenticationToken);
    }
}

export class UserReplacer extends ApplicationService {
    constructor(app) {
        super(app);
    }

    apply() {
        // get schema loader
        const schemaLoader = this.getApplication().getConfiguration().getStrategy(SchemaLoaderStrategy);
        // get model definition
        const model = schemaLoader.getModelDefinition('User');
        // get model class
        const loader = this.getApplication().getConfiguration().getStrategy(ModelClassLoaderStrategy);
        const UserClass = loader.resolve(model);
        // set user extensions
        UserClass.prototype.validate = UserExtensions.prototype.validate;
        UserClass.prototype.getProfile = UserExtensions.prototype.getProfile;

        const findAttribute = model.fields.find((field) => {
            return field.name === 'memberOf'
        });
        if (findAttribute == null) {
            model.fields.push({
                "name": "memberOf",
                "description": "The home institution of the user",
                "type": "Institute",
                "nullable": true
            });
            // upgrade version
            const { version } = model;
            // patch version
            model.version = inc(coerce(version), 'patch');
            model.eventListeners = model.eventListeners || [];
            schemaLoader.setModelDefinition(model);
        }
    }

}

