import { ApplicationService, DataError } from "@themost/common";
import { DataCacheStrategy, ModelClassLoaderStrategy, SchemaLoaderStrategy } from "@themost/data";
import { EdmMapping, DataObject, EdmType } from '@themost/data';
import { EnablePublicAttachment } from './models/EnablePublicAttachment';
class Course extends DataObject {

    @EdmMapping.func('VclpCourses', EdmType.CollectionOf('Object'))
    static async getVclpCourses(context) {
        const cache = context.getApplication().getConfiguration().getStrategy(DataCacheStrategy);
        const result = await cache.getOrDefault('/Courses/VclpCourses', () => {
            return context.getApplication().getService(function EpicurService() { }).getCourses(100);
        });
        return result;
    }

    @EdmMapping.func('CourseRegistry', EdmType.CollectionOf('OneRosterIncomingCourse'))
    static async getCourseRegistry(context) {
        return context.model('OneRosterIncomingCourse').asQueryable();
    }


    @EdmMapping.param('ID', EdmType.EdmInt32, false, false)
    @EdmMapping.action('VclpCourseReference', 'Object')
    static async getVclpCourseReference(context, id) {
        return await context.getApplication().getService(function EpicurService() { }).getCourseReferenceID(id);
    }

    /**
     * Adds an attachment
     * @param {*} file
     * @param {*=} extraAttributes
     */
    @EdmMapping.param('extraAttributes', 'Object', true, true)
    @EdmMapping.param('file', EdmType.EdmStream, false)
    @EdmMapping.action('AddAttachment', 'Attachment')
    async addAttachment(file, extraAttributes) {
        const attachment = Object.assign({
            name: file.contentFileName
        }, file, extraAttributes);
        const addAttachmentAsync = EnablePublicAttachment.prototype.addAttachment.bind(this);
        return await addAttachmentAsync(attachment);
    }
    /**
     * Removes an attachment
     * @param {*} attachment
     */
    @EdmMapping.param('attachment', 'Attachment', true, true)
    @EdmMapping.action('RemoveAttachment', 'Attachment')
    async removeAttachment(attachment) {
        const removeAttachmentAsync = EnablePublicAttachment.prototype.removeAttachment.bind(this);
        return await removeAttachmentAsync(attachment.id);
    }

}

export class CourseReplacer extends ApplicationService {
    constructor(app) {
        super(app);
    }

    apply() {
        // get schema loader
        const schemaLoader = this.getApplication().getConfiguration().getStrategy(SchemaLoaderStrategy);
        // get model definition
        const model = schemaLoader.getModelDefinition('Course');
        // get model class
        const loader = this.getApplication().getConfiguration().getStrategy(ModelClassLoaderStrategy);
        const CourseBase = loader.resolve(model);
        // extend class
        CourseBase.getVclpCourses = Course.getVclpCourses;
        CourseBase.getCourseRegistry = Course.getCourseRegistry;
        CourseBase.getVclpCourseReference = Course.getVclpCourseReference;
        CourseBase.prototype.addAttachment = Course.prototype.addAttachment;
        CourseBase.prototype.removeAttachment = Course.prototype.removeAttachment;
    }

}

