import { DataCacheStrategy, DataConfigurationStrategy } from '@themost/data';
import { Router } from 'express';
/**
 * 
 * @param {import('@themost/express').ExpressDataApplication} app 
 * @returns 
 */
function applicationConfigurationRouter(app) {
    const router = Router();

    router.use(function setContext(req, res, next) {
        // create context
        const context = app.createContext();
        Object.defineProperty(req, 'context', {
            enumerable: false,
            configurable: true,
            value: context
        });
        // finalize context
        res.on('end', () => {
            if (context) {
                context.finalize(() => {
                    //
                });
            }
        });
        return next();
    });

    router.get('/', (req, res) => {
        // get applications
        const fromCache = req.context.application.getConfiguration().getStrategy(DataCacheStrategy);
        fromCache.getOrDefault('/.well-known/application-configuration', async () => {
            const applications = await req.context.model('WebApplication').select(
                'name', 'alternateName', 'applicationSuite', 'applicationCategory', 'description', 'url', 'image'
            ).orderBy('position').silent().getItems();
            // get origin
            const issuer = req.context.application.getConfiguration().getSourceAt('settings/universis/api/origin');
            return {
                issuer,
                applications
            };
        }, 300).then((result) => {
            return res.json(result)
        }).catch((err) => {
            return next(err);
        });
    });

    return router;
}

export {
    applicationConfigurationRouter
}
