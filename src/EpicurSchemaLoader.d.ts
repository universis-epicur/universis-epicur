import {FileSchemaLoaderStrategy} from '@themost/data';
import { ConfigurationBase } from '@themost/common';
export declare class EpicurSchemaLoader extends FileSchemaLoaderStrategy {
    constructor(config: ConfigurationBase);
}