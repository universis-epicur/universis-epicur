import { ApplicationService, TraceUtils } from "@themost/common";
import './DataFilterExtensions';
import './FunctionContextExtensions';
import { EpicurSchemaReplacer } from './EpicurSchemaReplacer';
import { SchemaLoaderStrategy, ODataModelBuilder, DataTypeValidator } from "@themost/data";
import LocalScopeAccessConfiguration from './config/scope.access.json';
import { StudentPeriodRegistrationReplacer } from "./StudentPeriodRegistrationReplacer";
import { UserReplacer } from "./UserReplacer";
import { createClientAsync } from 'soap';
import { XSerializer, XDocument } from '@themost/xml';
import path from 'path';
import { CourseClassReplacer } from "./CourseClassReplacer";
import { CourseReplacer } from "./CourseReplacer";
import { InternshipReplacer } from "./InternshipReplacer";
import { InstructorReplacer } from "./InstructorReplacer";
export class EpicurService extends ApplicationService {
    constructor(app) {
        super(app);
        // get application schema loader
        const schemaLoader = app.getConfiguration().getStrategy(SchemaLoaderStrategy);
        // create replacer
        const replacer = new EpicurSchemaReplacer(app.getConfiguration());
        // get models
        const models = replacer.readSync();
        // loop throught replacer models
        models.forEach((model) => {
            // get local model definition
            const modelDefinition = replacer.getModelDefinition(model);
            // and replace root model definition
            schemaLoader.setModelDefinition(modelDefinition);
        });

        // add here core model replacers
        new StudentPeriodRegistrationReplacer(app).apply();
        new UserReplacer(app).apply();
        new CourseClassReplacer(app).apply();
        new InternshipReplacer(app).apply();
        new CourseReplacer(app).apply();
        new InstructorReplacer(app).apply();
        /**
         * get builder
         * @type ODataModelBuilder
         */
        const builder = app.getService(ODataModelBuilder);
        if (builder != null) {
            // cleanup builder and wait for next call
            builder.clean(true);
            builder.initializeSync();
        }

        if (app.container) {
            app.container.subscribe((container) => {
                if (container) {
                    // add extra scope access elements
                    const scopeAccess = app.getConfiguration().getStrategy(function ScopeAccessConfiguration() { });
                    if (scopeAccess != null) {
                        scopeAccess.elements.push.apply(scopeAccess.elements, LocalScopeAccessConfiguration);
                    }
                }
            });
        }


        const options = this.getApplication().getConfiguration().getSourceAt('settings/universis/epicur');
        if (options == null) {
            TraceUtils.warn('(EpicurService) Service configuration cannot be found or is inaccessible. Epicur service will fail to communicate with VCLP server.')
        }
        // get service options
        Object.defineProperty(this, 'options', {
            enumerable: false,
            configurable: false,
            value: options || {
                vclp: {
                    server_uri: null,
                    service_uri: null
                }
            }
        });

        // register templates
        // get MailTemplateService by name
        const mailTemplateService = this.getApplication().getService(function MailTemplateService() { });
        if (mailTemplateService != null) {
            mailTemplateService.extraTemplates.push({
                name: 'new-message',
                path: path.resolve(__dirname, './templates/new-message/html.ejs')
            });
            mailTemplateService.extraTemplates.push({
                name: 'direct-message',
                path: path.resolve(__dirname, './templates/direct-message/html.ejs')
            });
            mailTemplateService.extraTemplates.push({
                name: 'register-status-change',
                path: path.resolve(__dirname, './templates/register-status-change/html.ejs')
            });
            mailTemplateService.extraTemplates.push({
                name: 'application-accepted',
                path: path.resolve(__dirname, './templates/application-accepted/html.ejs')
            });
        }

    }

    async getVclpServiceClient() {
        if (this._serviceClient == null) {
            if (this.options.vclp == null) {
                throw new Error('Invalid application configuration. VCLP configuration is empty.');
            }
            if (this.options.vclp.service_uri == null) {
                throw new Error('Invalid application configuration. VCLP server uri has not been set.');
            }
            const client = await createClientAsync(this.options.vclp.service_uri);
            Object.defineProperty(this, '_serviceClient', {
                enumerable: false,
                configurable: true,
                value: client
            });
        }
        return this._serviceClient;
    }

    getServiceToken() {
        return new Promise((resolve, reject) => {
            return this.getVclpServiceClient().then((client) => {
                return client.loginAsync({
                    client: this.options.vclp.client_id,
                    username: this.options.vclp.user,
                    password: this.options.vclp.password
                }).then((result) => {
                    return resolve(result[0]);
                });
            }).catch((err) => {
                return reject(err);
            });
        });
    }

    /**
     * Validates a user against VCLP server
     * @param {DataContext} context - The user email to validate 
     * @param {string} name - The user email to validate 
     */
    async validateUser(context, name) {
        const validator = new DataTypeValidator('Email');
        validator.setContext(context);
        if (validator.validateSync(name) != null) {
            return Promise.reject(new Error('Invalid parameter. Search parameter must be a valid email address'));
        }
        return new Promise((resolve, reject) => {
            return this.getVclpServiceClient().then((client) => {
                return this.getServiceToken().then((token) => {
                    return client.searchUserAsync({
                        sid: token.sid,
                        key_fields: [
                            'email'
                        ],
                        query_operator: 'and',
                        key_values: [
                            name
                        ],
                        attach_roles: 0,
                        active: 1
                    }).then((result) => {
                        // get xml result
                        const resultXmlString = result && result[0] && result[0].user_xml && result[0].user_xml.$value;
                        if (resultXmlString == null) {
                            return resolve();
                        }
                        // deserialize xml
                        const doc = XDocument.loadXML(resultXmlString);
                        // eslint-disable-next-line no-unused-vars
                        for (const node of doc.childNodes) {
                            // search by name because of comments
                            if (node && node.localName === 'Users') {
                                // get object
                                // eslint-disable-next-line no-unused-vars
                                for (const childNode of node.childNodes) {
                                    if (childNode && childNode.localName === 'User') {
                                        const user = XSerializer.deserialize(childNode);
                                        // get user id (extracted from string value)
                                        const matches = /\d+$/.exec(childNode.getAttribute('Id'));
                                        if (matches && matches[0]) {
                                            user.ID = matches[0];
                                        }
                                        return resolve(user);
                                    }
                                }
                                // and return
                                return resolve();
                            }
                        }
                        return resolve();
                    })
                });
            }).catch((err) => {
                return reject(err);
            });
        });
    }

    /**
     * 
     * @param {number} course_id 
     * @param {*=} options 
     */
    async getCourse(course_id, options) {
        const client = await this.getVclpServiceClient();
        let serviceToken = options && options.token;
        if (serviceToken == null) {
            serviceToken = await this.getServiceToken();
        }
        const result = await client.getCourseXMLAsync({
            sid: serviceToken.sid,
            course_id: parseInt(course_id, 10)
        });
        return result;
    }

    /**
     * 
     * @param {*} course_id 
     * @param {*=} options 
     * @returns {{ID: any, ReferenceID: any}}
     */
    async getCourseReferenceID(course_id, options) {
        const client = await this.getVclpServiceClient();
        let serviceToken = options && options.token;
        if (serviceToken == null) {
            serviceToken = await this.getServiceToken();
        }
        const result = await client.getRefIdsByObjIdAsync({
            sid: serviceToken.sid,
            obj_id: course_id
        });
        if (Array.isArray(result) && result[0]
            && result[0].ref_ids
            && result[0].ref_ids.item) {
            return {
                ID: course_id,
                ReferenceID: result[0].ref_ids.item.$value
            };
        }
        return null;
    }

    /**
     * 
     * @param {*} ref_id 
     * @param {*=} options 
     */
    async getCourseRoles(ref_id, options) {
        const client = await this.getVclpServiceClient();
        let serviceToken = options && options.token;
        if (serviceToken == null) {
            serviceToken = await this.getServiceToken();
        }
        const result = await client.getLocalRolesAsync({
            sid: serviceToken.sid,
            ref_id: ref_id
        });
        let finalResult = [];
        if (result && result[0] && result[0].role_xml) {
            const resultXmlString = result[0].role_xml.$value;
            const doc = XDocument.loadXML(resultXmlString);
            // eslint-disable-next-line no-unused-vars
            for (let node of doc.childNodes) {
                // search by name because of comments
                if (node && node.localName === 'Objects') {
                    // get object
                    finalResult = [];
                    // eslint-disable-next-line no-unused-vars
                    for (let childNode of node.childNodes) {
                        if (childNode.localName === 'Object') {
                            const child = XSerializer.deserialize(childNode);
                            child.ID = childNode.getAttribute('obj_id');
                            child.Type = childNode.getAttribute('type');
                            finalResult.push(child);
                        }
                    }
                    break;
                }
            }
        }
        return finalResult;
    }

    /**
     * @param {*} ref_id
     * @param {{Title: string, Description: string}} role 
     * @param {*=} options 
     */
    async addCourseRole(ref_id, role, options) {
        const client = await this.getVclpServiceClient();
        let serviceToken = options && options.token;
        if (serviceToken == null) {
            serviceToken = await this.getServiceToken();
        }
        Object.assign(role, {
            ImportId: null
        });
        // format data
        const doc = XSerializer.serialize([role], {
            root: 'Objects'
        });
        // eslint-disable-next-line no-unused-vars
        for (let child of doc.childNodes) {
            child.setAttribute('type', 'role');
            child.setAttribute('obj_id', '0');
        }
        const roleXmlString = doc.outerXML();
        const result = await client.addRoleAsync({
            sid: serviceToken.sid,
            target_id: ref_id,
            obj_xml: `${roleXmlString}`
        });
        Object.assign(role, {
            ID: result[0].role_ids.item.$value
        });
        await this.addCourseRoleDefaultPermissions(ref_id, role.ID, options);
        return role;
    }

    /**
     * @param {*} ref_id
     * @param {*} role_id
     * @param {*=} options 
     */
    async removeCourseRole(role_id, options) {
        const client = await this.getVclpServiceClient();
        let serviceToken = options && options.token;
        if (serviceToken == null) {
            serviceToken = await this.getServiceToken();
        }
        const result = await client.deleteRoleAsync({
            sid: serviceToken.sid,
            role_id: role_id
        });
        return result;
    }

    async addCourseRoleDefaultPermissions(ref_id, role_id, options) {
        const client = await this.getVclpServiceClient();
        let serviceToken = options && options.token;
        if (serviceToken == null) {
            serviceToken = await this.getServiceToken();
        }
        const result = await client.grantPermissionsAsync({
            sid: serviceToken.sid,
            ref_id: ref_id,
            role_id: role_id,
            operations: [2, 3]
        });
        return result;
    }

    async assignUserRole(role_id, user_id, options) {
        const client = await this.getVclpServiceClient();
        let serviceToken = options && options.token;
        if (serviceToken == null) {
            serviceToken = await this.getServiceToken();
        }
        const result = await client.addUserRoleEntryAsync({
            sid: serviceToken.sid,
            user_id: user_id,
            role_id: role_id
        });
        return result;
    }

    async getCourses(parent_ref_id, options) {
        const client = await this.getVclpServiceClient();
        let serviceToken = options && options.token;
        if (serviceToken == null) {
            serviceToken = await this.getServiceToken();
        }
        const serviceUser = await this.getUserByName(this.options.vclp.user, {
            token: serviceToken
        });
        const result = await client.getXMLTreeAsync({
            sid: serviceToken.sid,
            ref_id: parent_ref_id,
            types: [],
            user_id: serviceUser.ID,
        });
        const finalResult = [];
        if (result && result[0]) {
            const doc = XDocument.loadXML(result[0].object_xml.$value);
            // eslint-disable-next-line no-unused-vars
            for (let node of doc.childNodes) {
                // search by name because of comments
                if (node && node.localName === 'Objects') {
                    // get object
                    // eslint-disable-next-line no-unused-vars
                    for (let childNode of node.childNodes) {
                        if (childNode.localName === 'Object') {
                            const child = XSerializer.deserialize(childNode);
                            child.ID = childNode.getAttribute('obj_id');
                            child.Type = childNode.getAttribute('type');
                            finalResult.push(child);
                        }
                    }
                    break;
                }
            }
        }
        return finalResult;
    }

    /**
     * 
     * @param {{Login: string,Firstname: string, Lastname: string, Email: string, Institution: string, Active: boolean, AuthMode: any}} user 
     * @param {{token: string}=} options
     */
    async addUser(user, options) {
        const client = await this.getVclpServiceClient();
        let serviceToken = options && options.token;
        if (serviceToken == null) {
            serviceToken = await this.getServiceToken();
        }
        class User { }
        const newUser = Object.assign(new User(), user);
        const doc = XSerializer.serialize([newUser], {
            root: 'Users',
        });
        let node = doc.selectSingleNode('User/AuthMode');
        if (node != null) {
            node.setAttribute('type', 'shibboleth');
        }
        // add role
        const roleNode = doc.selectSingleNode('User/Role');
        if (roleNode != null) {
            roleNode.setAttribute('Id', 'il_0_role_5'); // Guest
            roleNode.setAttribute('Type', 'Global');
        }
        const userXML = doc.outerXML();
        TraceUtils.debug('EpicurService.addUser()', userXML);
        const result = await client.importUsersAsync({
            sid: serviceToken.sid,
            folder_id: -1,
            usr_xml: userXML,
            conflict_rule: -1,
            send_account_mail: -1
        });
        TraceUtils.debug('EpicurService.addUser()', JSON.stringify(result));
        const resultXML = result[0].protocol.$value;
        const resultDoc = XDocument.loadXML(resultXML);
        const finalResult = XSerializer.deserialize(resultDoc);
        return finalResult;
    }

    /**
     * 
     * @param {*} user_id
     * @param {{token: string}=} options
     */
    async removeUser(user_id, options) {
        const client = await this.getVclpServiceClient();
        let serviceToken = options && options.token;
        if (serviceToken == null) {
            serviceToken = await this.getServiceToken();
        }
        const result = await client.deleteUserAsync({
            sid: serviceToken.sid,
            user_id: user_id
        });
        TraceUtils.debug('EpicurService.removeUser()', JSON.stringify(result));
        if (result[0] && result[0].success) {
            return result[0].success;
        }
        return false;
    }

    /**
     * @param {*} user_id
     * @param {*=} options 
     */
    async getUser(user_id, options) {
        const client = await this.getVclpServiceClient();
        let serviceToken = options && options.token;
        if (serviceToken == null) {
            serviceToken = await this.getServiceToken();
        }
        const result = await client.getUserAsync({
            sid: serviceToken.sid,
            user_id: user_id
        });
        return result[0] && result[0].user_data;
    }

    /**
     * @param {string} name
     * @param {{token: string}=} options
     */
    async getUserByName(name, options) {
        const client = await this.getVclpServiceClient();
        let token = options && options.token;
        if (token == null) {
            token = await this.getServiceToken();
        }
        const result = await client.searchUserAsync({
            sid: token.sid,
            key_fields: [
                'login'
            ],
            query_operator: 'and',
            key_values: [
                name
            ],
            attach_roles: 0,
            active: 1
        });
        // get xml result
        const resultXmlString = result && result[0] && result[0].user_xml && result[0].user_xml.$value;
        if (resultXmlString == null) {
            return;
        }
        // deserialize xml
        const doc = XDocument.loadXML(resultXmlString);
        // eslint-disable-next-line no-unused-vars
        for (const node of doc.childNodes) {
            // search by name because of comments
            if (node && node.localName === 'Users') {
                // get object
                // eslint-disable-next-line no-unused-vars
                for (const childNode of node.childNodes) {
                    if (childNode && childNode.localName === 'User') {
                        const user = XSerializer.deserialize(childNode);
                        // get user id (extracted from string value)
                        const matches = /\d+$/.exec(childNode.getAttribute('Id'));
                        if (matches && matches[0]) {
                            user.ID = matches[0];
                        }
                        return user;
                    }
                }
            }
        }
    }

    /**
     * @param {*} user_id
     * @param {*=} options 
     */
    async getUserRoles(user_id, options) {
        const client = await this.getVclpServiceClient();
        let serviceToken = options && options.token;
        if (serviceToken == null) {
            serviceToken = await this.getServiceToken();
        }
        const result = await client.getUserRolesAsync({
            sid: serviceToken.sid,
            user_id: user_id
        });
        return result[0] && result[0].role_xml;
    }

}