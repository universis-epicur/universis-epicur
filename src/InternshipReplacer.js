import { ApplicationService } from "@themost/common";
import { ModelClassLoaderStrategy, SchemaLoaderStrategy, EdmMapping, EdmType } from "@themost/data";
import path from 'path';

class Internship {
    @EdmMapping.func('available', EdmType.CollectionOf('Internship'))
    static async getAvailableInternships(department) {
        return this.context.model('AvailableInternship')
            .where('department').equal(department)
            .prepare();
    }
}

export class InternshipReplacer extends ApplicationService {
    constructor(app) {
        super(app);
    }

    apply() {
        // get schema loader
        const schemaLoader = this.getApplication().getConfiguration().getStrategy(SchemaLoaderStrategy);
        // get model definition
        const model = schemaLoader.getModelDefinition('Internship');
        // add listener
        const setAvailableInternshipListener = path.resolve(__dirname, 'listeners/set-available-internship-listener');
        model.eventListeners = model.eventListeners || [];
        const find = model.eventListeners.find((item) => {
            return item.type === setAvailableInternshipListener;
        });
        if (find == null) {
            model.eventListeners.push({
                type: setAvailableInternshipListener
            });
            schemaLoader.setModelDefinition(model);
        }
        // get model class
        const loader = this.getApplication().getConfiguration().getStrategy(ModelClassLoaderStrategy);
        const InternshipClass = loader.resolve(model);
        // set user extensions
        InternshipClass.prototype.getAvailableInternships = Internship.prototype.getAvailableInternships;
    }

}

