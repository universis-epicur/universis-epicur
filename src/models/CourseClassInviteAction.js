import CourseClasRegisterAction from './CourseClassRegisterAction';
import { EdmMapping } from '@themost/data';

@EdmMapping.entityType('CourseClassInviteAction')
class CourseClassInviteAction extends CourseClasRegisterAction {
    constructor() {
        super()
    }
}

export {
    CourseClassInviteAction
}