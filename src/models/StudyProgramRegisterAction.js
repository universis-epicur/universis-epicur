import { EdmMapping, EdmType } from "@themost/data";
import EnableAttachmentModel from './EnableAttachmentModel';
import archiver from 'archiver';
import { promisify } from 'util';
import fs from 'fs';
import '@themost/promise-sequence';

class StudyProgramRegisterAction extends EnableAttachmentModel {
    constructor() {
        super();
    }
    /**
     * Adds an attachment
     * @param {*} file
     * @param {*=} extraAttributes
     */
    @EdmMapping.param('extraAttributes', 'Object', true, true)
    @EdmMapping.param('file', EdmType.EdmStream, false)
    @EdmMapping.action('AddAttachment', 'Attachment')
    async addAttachment(file, extraAttributes) {
        const attachment = Object.assign({
            name: file.contentFileName
        }, file, extraAttributes);
        return await super.addAttachment(attachment);
    }
    /**
     * Removes an attachment
     * @param {*} attachment
     */
    @EdmMapping.param('attachment', 'Attachment', true, true)
    @EdmMapping.action('RemoveAttachment', 'Attachment')
    async removeAttachment(attachment) {
        return await super.removeAttachment(attachment.id);
    }

    /**
     * Set preferred course classes
     * @param {Array<any>} items
     */
    @EdmMapping.param('items', EdmType.CollectionOf('CourseClassRegisterAction'), true, true)
    @EdmMapping.action('CourseRegistrations', EdmType.CollectionOf('CourseClassRegisterAction'))
    async setCourseRegistrations(items) {
        // set initiator
        items.forEach((item) => {
            item.initiator = {
                id: this.getId()
            };
        });
        // get invitations
        const invitations = items.filter((item) => {
            return item.additionalType === 'CourseClassInviteAction';
        });
        if (invitations.length) {
            // and save them
            await this.context.model('CourseClassInviteAction').save(invitations);
        }
        // save other course class registrations
        return this.context.model('CourseClassRegisterAction').save(items.filter((item) => {
            return item.additionalType !== 'CourseClassInviteAction';
        }));
    }

    /**
     * Set preferred internships
     * @param {Array<any>} items
     */
    @EdmMapping.param('items', EdmType.CollectionOf('InternshipRegisterAction'), true, true)
    @EdmMapping.action('InternshipRegistrations', EdmType.CollectionOf('InternshipRegisterAction'))
    async setInternshipRegistrations(items) {
        // set initiator
        items.forEach((item) => {
            item.initiator = {
                id: this.getId()
            };
        });
        return this.context.model('InternshipRegisterAction').save(items);
    }

    /**
     * Add action messages
     * @param {Array<any>} items
     */
    @EdmMapping.param('item', 'StudyProgramRegisterActionMessage', true, true)
    @EdmMapping.action('messages', 'StudyProgramRegisterActionMessage')
    async postMessages(item) {
        // set initiator
        item.action = this.getId();
        return this.context.model('StudyProgramRegisterActionMessage').save(item);
    }

    /**
     * Gets item review
     */
    @EdmMapping.func('review', 'StudyProgramRegisterActionReview')
    getReview() {
        return this.context.model('StudyProgramRegisterActionReview')
            .where('itemReviewed').equal(this.getId()).prepare();
    }

    /**
     * Set item review
     * @param {*} item
     */
    @EdmMapping.param('item', 'StudyProgramRegisterActionReview', true, true)
    @EdmMapping.action('review', 'StudyProgramRegisterActionReview')
    async setReview(item) {
        const StudyProgramRegisterActionReviews = this.context.model('StudyProgramRegisterActionReview');
        // infer object state
        const currentReview = await StudyProgramRegisterActionReviews.where('itemReviewed').equal(this.getId()).getItem();
        if (currentReview == null) {
            if (item == null) {
                return;
            }
            // a new item is going to be inserted
            delete item.id;
            // set reviewed item
            item.itemReviewed = this.getId();
        } else {
            if (item == null) {
                // delete review
                StudyProgramRegisterActionReviews.remove(currentReview);
            }
            // up
            item.id = currentReview.id;
            // set reviewed item
            item.itemReviewed = this.getId();
        }
        return StudyProgramRegisterActionReviews.save(item);
    }

    /**
     * Adds an attachment
     * @param {*} file
     * @param {*=} extraAttributes
     */
    @EdmMapping.param('message', 'Object', true, true)
    @EdmMapping.param('attachment', EdmType.EdmStream, false)
    @EdmMapping.action('SendMessage', 'RegisterActionMessage')
    sendMessage(attachment, message) {
        const thisArg = this;
        return new Promise((resolve, reject) => {
            this.context.db.executeInTransaction((cb) => {
                (async () => {
                    const StudyProgramRegisterActionMessage = thisArg.context.model('StudyProgramRegisterActionMessage');
                    const newMessage = StudyProgramRegisterActionMessage.convert(message);
                    Object.assign(newMessage, {
                        action: thisArg.getId()
                    });
                    // add message
                    await StudyProgramRegisterActionMessage.save(newMessage);
                    // if message has an attachment
                    if (attachment) {
                        // prepare attachment data
                        const newAttachment = Object.assign({
                            name: attachment.contentFileName
                        }, attachment);
                        // and add attachment
                        await newMessage.addAttachment(newAttachment)
                    }
                })().then(() => {
                    return cb();
                }).catch((err) => {
                    return cb(err);
                });
            }, (err) => {
                if (err) {
                    return reject(err);
                }
                return resolve(message);
            })
        });
    }

    /**
     * Gets a compressed file with attachments
     */
    @EdmMapping.func('AttachmentsAsArchive', EdmType.EdmStream)
    getAttachmentsAsArchive() {
        return this.property('attachments').getItems().then((attachments) => {
            return new Promise((resolve, reject) => {
                const service = this.context.getApplication().getService(function PrivateContentService() { });
                // eslint-disable-next-line no-unused-vars
                const createReadStreamAsync = promisify(service.createReadStream.bind(service));
                const archive = archiver('zip');
                const context = this.context;
                const output = fs.createWriteStream(`tmp/archive.zip`);
                archive.pipe(output);
                output.on('close', () => resolve(archive));
                archive.on('error', (err) => reject(err));
                const sources = attachments.map((item) => {
                    return () => createReadStreamAsync(context, item);
                });
                Promise.sequence(sources).then((readStreams) => {
                    readStreams.forEach((readStream) => {
                        archive.append(readStream)
                    });
                    // finalize
                    archive.finalize();
                });
            });
        });

    }


}

module.exports = StudyProgramRegisterAction;
