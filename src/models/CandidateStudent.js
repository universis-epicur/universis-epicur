import { DataObject, EdmMapping, EdmType } from "@themost/data";
class CandidateStudent extends DataObject {

    /**
     * Adds an attachment
     * @param {*} file
     * @param {*=} extraAttributes
     */
    @EdmMapping.param('message', 'Object', true, true)
    @EdmMapping.param('attachment', EdmType.EdmStream, false)
    @EdmMapping.action('SendMessage', 'CandidateStudentMessage')
    sendMessage(attachment, message) {
        const thisArg = this;
        return new Promise((resolve, reject) => {
            this.context.db.executeInTransaction((cb) => {
                (async () => {
                    const CandidateStudentMessage = thisArg.context.model('CandidateStudentMessage');
                    const newMessage = CandidateStudentMessage.convert(message);
                    Object.assign(newMessage, {
                        action: thisArg.getId()
                    });
                    // add message
                    await CandidateStudentMessage.save(newMessage);
                    // if message has an attachment
                    if (attachment) {
                        // prepare attachment data
                        const newAttachment = Object.assign({
                            name: attachment.contentFileName
                        }, attachment);
                        // and add attachment
                        await newMessage.addAttachment(newAttachment)
                    }
                })().then(() => {
                    return cb();
                }).catch((err) => {
                    return cb(err);
                });
            }, (err) => {
                if (err) {
                    return reject(err);
                }
                return resolve(message);
            })
        });
    }

    /**
     * Add action messages
     * @param {Array<any>} items
     */
    @EdmMapping.param('item', 'CandidateStudentMessage', true, true)
    @EdmMapping.action('messages', 'CandidateStudentMessage')
    async postMessages(item) {
        // set initiator
        item.action = this.getId();
        return this.context.model('CandidateStudentMessage').save(item);
    }

}

module.exports = CandidateStudent;