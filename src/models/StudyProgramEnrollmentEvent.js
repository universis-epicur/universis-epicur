import { DataObject } from '@themost/data';


class StudyProgramEnrollmentEvent extends DataObject {
    constructor() {
        super();
    }
}

export {
    StudyProgramEnrollmentEvent
}
