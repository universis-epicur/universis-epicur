import {DataObject} from '@themost/data';
import {TraceUtils, RandomUtils, DataNotFoundError, HttpBadRequestError} from "@themost/common";
import path from 'path';
import { PublicContentService } from '../PublicContentService';

class MimeTypeError extends HttpBadRequestError {
    constructor(message) {
        super(message);
        this.code = "E_MIME";
    }
}

class EnablePublicAttachment extends DataObject {

    constructor() {
        super();
    }

    addAttachment(file) {
        const context = this.context;
        const self = this;
        let finalResult;
        return new Promise((resolve, reject)=> {
            this.context.db.executeInTransaction((callback)=> {
                if (typeof file.originalname === 'string') {
                    file.name = file.originalname.replace(/\.[A-Z0-9]+$/ig, function(x) { return x.toLowerCase(); });
                }
                const mime = file.mimetype || file.contentType;
                if (mime == null) {
                    return reject(new MimeTypeError('The specified file type is not supported by the system.'))
                }
                const addAttributes ={};
                const attributeNames = context.model('PublicAttachment').attributeNames;
                attributeNames.forEach((attribute)=> {
                    if (Object.prototype.hasOwnProperty.call(file, attribute)) {
                        addAttributes[attribute] = file[attribute];
                    }
                });
                // add defaults
                const alternateName = addAttributes.name.replace(/(\.(\w+))$/, '-' + RandomUtils.randomChars(8) + '$1');
                const url = `/api/content/public/${alternateName}`;
                const additionalType = 'PublicAttachment';
                const newAttachment = Object.assign(addAttributes, {
                    contentType:mime,
                    additionalType,
                    alternateName,
                    url
                });
                const svc = context.getApplication().getService(PublicContentService);
                if (svc == null) {
                    return callback(new Error('Public content service is not available.'));
                }
                // get file path
                const filePath = (file.destination && file.filename) ? path.resolve(file.destination, file.filename) : file.path;
                // get attachments collection
                return svc.copyFrom(context,  filePath, newAttachment, function(err) {
                    if (err) {
                        TraceUtils.error(err);
                        return callback(err);
                    }
                    const attachments = self.property('attachments');
                    return attachments.insert(newAttachment, (err)=> {
                        if (err) {
                            return callback(err);
                        }
                        return svc.resolveUrl(context, newAttachment, function(err, url) {
                            if (err) {
                                return callback(err);
                            }
                            finalResult =Object.assign({
                            }, newAttachment, {
                                url: url,
                                name: newAttachment.filename
                            });
                            return callback();
                        });
                    });
                });

                
            }, (err)=> {
                if (err) {
                    return reject(err);
                }
                return resolve(finalResult);
            });
        })

    }

    removeAttachment(id) {
        const context = this.context;
        const self = this;
        let result;
        return new Promise((resolve, reject)=> {
            context.db.executeInTransaction((callback)=> {
                //get attachment
                return context.model('PublicAttachment').where('id').equal(id).getItem().then((attachment)=> {
                   if (attachment) {
                       /**
                        * @type {import('@themost/data').DataObjectJunction}
                        */
                       const attachments = self.property('attachments');
                       // validate attachment model
                       return attachments.remove(attachment,(err)=> {
                           //remove attachment
                           if (err) {
                               return callback(err);
                           }
                           return context.model('PublicAttachment').silent().remove(attachment).then(()=> {
                               result = attachment;
                               return callback();
                           }).catch((err)=> {
                               return callback(err);
                           });
                       });
                   }
                   return callback(new DataNotFoundError());
                }).catch((err)=> {
                    return callback(err);
                });
            }, (err)=> {
                if (err) {
                    return reject(err);
                }
                return resolve(result);
            });
        });
    }

}

export {
    EnablePublicAttachment
}

