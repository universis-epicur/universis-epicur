import { DataObject, EdmMapping, EdmType } from '@themost/data';
import { EnablePublicAttachment } from './EnablePublicAttachment';

@EdmMapping.entityType('Institute')
class Institute extends EnablePublicAttachment {
    /**
     * Adds an attachment
     * @param {*} file
     * @param {*=} extraAttributes
     */
     @EdmMapping.param('extraAttributes', 'Object', true, true)
     @EdmMapping.param('file', EdmType.EdmStream, false)
     @EdmMapping.action('AddAttachment', 'Attachment')
     async addAttachment(file, extraAttributes) {
         const attachment = Object.assign({
             name: file.contentFileName
         }, file, extraAttributes);
         return await super.addAttachment(attachment);
     }
     /**
      * Removes an attachment
      * @param {*} attachment
      */
     @EdmMapping.param('attachment', 'Attachment', true, true)
     @EdmMapping.action('RemoveAttachment', 'Attachment')
     async removeAttachment(attachment) {
         return await super.removeAttachment(attachment.id);
     }
}

export {
    Institute
}