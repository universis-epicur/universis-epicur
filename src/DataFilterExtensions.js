import { DataFilterResolver } from '@themost/data/data-filter-resolver';
import { Guid } from '@themost/common';

Object.assign(DataFilterResolver.prototype, {
    /**
     * @this DataFilterResolver
     * @param {*} callback 
     */
    whoami: function (callback) {
        const context = this.context;
        if (context == null) {
            return callback(new Error('Current context cannot be empty.'));
        }
        if (context.user && context.user.name) {
            return callback(null, context.user && context.user.name);
        }
        return callback(new Error('Current user cannot be empty at this context.'));
    },
    userAlternateName: function (callback) {
        const context = this.context;
        if (context == null) {
            return callback(new Error('Current context cannot be empty.'));
        }
        if (context.user && context.user.name) {
            return context.model('User').where('name')
                .equal(context.user.name).select('alternateName')
                .value().then((value) => {
                    if (value) {
                        return callback(null, value);
                    }
                    const randomGuid = Guid.newGuid().toString().replace(/\\-/g, '');
                    return callback(null, randomGuid);
                }).catch((err) => {
                    return callback(err);
                });
        }
        return callback(new Error('Current user cannot be empty at this context.'));
    },
    /**
     * @this DataFilterResolver
     * @param {*} callback 
     */
     institute: function (callback) {
        const context = this.context;
        if (context == null) {
            return callback(new Error('Current context cannot be empty.'));
        }
        if (context.user && context.user.name) {
            return context.model('User').select('memberOf/id')
            .where('name').equal(context.user.name)
            .value().then((value) => {
                return callback(null, value);
            }).catch((err) => {
                return callback(err);
            });
        }
        return callback(new Error('Current user cannot be empty at this context.'));
    },
});