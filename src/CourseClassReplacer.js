import { ApplicationService, DataError } from "@themost/common";
import { ModelClassLoaderStrategy, SchemaLoaderStrategy } from "@themost/data";
import { EdmMapping, DataObject, EdmType } from '@themost/data';

class CourseClass extends DataObject {
    @EdmMapping.param('message', 'Object', true, true)
    @EdmMapping.param('attachment', EdmType.EdmStream, true)
    @EdmMapping.action('SendMessage', 'InstructorMessage')
    async sendMessage(attachment, message) {
        const thisArg = this;
        // validate instructor
        if (message.instructor == null) {
            throw new DataError('E_MESSAGE_INSTRUCTOR', 'Course class instructor may not be empty at this context', null, 'InstructorMessage', 'instructor');
        }
        // get instructor and prepare operation
        const instructor = await this.context.model('Instructor')
            .find(message.instructor).expand('user')
            .silent().getItem();
        if (instructor == null) {
            throw new DataError('E_INSTRUCTOR', 'Instructor cannot be found or is inaccessible', null, 'InstructorMessage', 'instructor');
        }
        // validate that instructor belongs to this course class
        const isCourseClassInstructor = await this.property('instructors')
            .where('instructor').equal(instructor)
            .silent().count();
        if (isCourseClassInstructor === 0) {
            throw new DataError('E_CLASS_INSTRUCTOR', 'Course class instructor cannot be found or is inaccessible', null, 'CourseClassInstructor', 'instructor');
        }
        // get about course class data
        const studentCourseClass = await this.context.model('StudentCourseClass')
            .where('student/user/name').equal(this.context.user.name)
            .select('courseClass/id as id',
                'courseClass/title as title',
                'student/person/givenName as givenName',
                'student/person/familyName as familyName',
            ).getItem();
        if (studentCourseClass == null) {
            throw new DataError('E_CLASS_STUDENT', 'Student class cannot be found or is inaccessible', null, 'StudentCourseClass', 'student');
        }
        // try to update user description
        const user = await this.context.model('User').where('name').equal(this.context.user.name).getItem();
        if (user != null) {
            if (user.description == null) {
                await this.context.model('User').silent().save({
                    id: user.id,
                    description: `${studentCourseClass.givenName} ${studentCourseClass.familyName}`
                });
            }
        }

        // send message
        const newItem = Object.assign(message, {
            sender: {
                name: this.context.user.name
            },
            instructor: instructor.id,
            recipient: instructor.user.id,
            about: `CourseClasses/${studentCourseClass.id}/${studentCourseClass.title}`
        });
        await new Promise((resolve, reject) => {
            this.context.db.executeInTransaction((cb) => {
                (async () => {
                    const InstructorMessage = thisArg.context.model('InstructorMessage');
                    const newMessage = InstructorMessage.convert(newItem);
                    // add message
                    await InstructorMessage.silent().save(newMessage);
                    // if message has an attachment
                    if (attachment) {
                        // prepare attachment data
                        const newAttachment = Object.assign({
                            name: attachment.contentFileName
                        }, attachment);
                        // and add attachment
                        await newMessage.addAttachment(newAttachment)
                    }
                })().then(() => {
                    return cb();
                }).catch((err) => {
                    return cb(err);
                });
            }, (err) => {
                if (err) {
                    return reject(err);
                }
                return resolve();
            });
        });
    }
}

export class CourseClassReplacer extends ApplicationService {
    constructor(app) {
        super(app);
    }

    apply() {
        // get schema loader
        const schemaLoader = this.getApplication().getConfiguration().getStrategy(SchemaLoaderStrategy);
        // get model definition
        const model = schemaLoader.getModelDefinition('CourseClass');
        // get model class
        const loader = this.getApplication().getConfiguration().getStrategy(ModelClassLoaderStrategy);
        const CourseClassBase = loader.resolve(model);
        // extend class
        CourseClassBase.prototype.sendMessage = CourseClass.prototype.sendMessage;
    }

}

