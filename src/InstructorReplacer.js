import { ApplicationService } from "@themost/common";
import { ModelClassLoaderStrategy, SchemaLoaderStrategy, EdmMapping, EdmType } from "@themost/data";

class Instructor {
    @EdmMapping.func('CourseProposals', EdmType.CollectionOf('CourseProposal'))
    async getCourseProposals() {
        return this.context.model('CourseProposals').where('owner/name').equal(
            this.context.user.name
        ).prepare();
    }
    @EdmMapping.param('courseProposal', 'CourseProposal', false, true)
    @EdmMapping.action('CourseProposals', 'CourseProposal')
    async postCourseProposal(courseProposal) {
        return this.context.model('CourseProposals').save(courseProposal);
    }
}

class InstructorReplacer extends ApplicationService {
    constructor(app) {
        super(app);
    }

    apply() {
        // get schema loader
        const schemaLoader = this.getApplication().getConfiguration().getStrategy(SchemaLoaderStrategy);
        // get model definition
        const model = schemaLoader.getModelDefinition('Instructor');
        // get model class
        const loader = this.getApplication().getConfiguration().getStrategy(ModelClassLoaderStrategy);
        const InstructorClass = loader.resolve(model);
        // set user extensions
        const { getCourseProposals, postCourseProposal } = Instructor.prototype;
        Object.assign(InstructorClass.prototype, {
            getCourseProposals,
            postCourseProposal
        });
    }
}

export {
    InstructorReplacer
}

