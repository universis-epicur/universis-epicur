import { OneRosterService, OneRosterOrgProvider,
    OneRosterCourseProvider, OneRosterClassProvider, OneRosterUserProvider, OneRosterEnrollmentProvider } from '@universis/one-roster';
import { EpicurOneRosterOrgProvider } from './OneRosterOrgProvider';
import { EpicurOneRosterCourseProvider } from './OneRosterCourseProvider';
import { EpicurOneRosterClassProvider } from './OneRosterClassProvider';
import { EpicurOneRosterUserProvider } from './OneRosterUserProvider';
import { EpicurOneRosterEnrollmentProvider } from './OneRosterEnrollmentProvider';
import { OneRosterResultMetadataReplacer } from './OneRosterResultMetadataReplacer';

class EpicurOneRosterService extends OneRosterService {
    constructor(app) {
        super(app);
        this.useStrategy(OneRosterOrgProvider, EpicurOneRosterOrgProvider);
        this.useStrategy(OneRosterCourseProvider, EpicurOneRosterCourseProvider);
        this.useStrategy(OneRosterClassProvider, EpicurOneRosterClassProvider);
        this.useStrategy(OneRosterUserProvider, EpicurOneRosterUserProvider);
        this.useStrategy(OneRosterEnrollmentProvider, EpicurOneRosterEnrollmentProvider);
        // upgrade OneRosterResultMetadata
        new OneRosterResultMetadataReplacer(app).apply();
        
    }
}

export {
    EpicurOneRosterService
}