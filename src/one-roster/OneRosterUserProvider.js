import { OneRosterUserProvider }  from '@universis/one-roster';
import Chance from 'chance';

class EpicurOneRosterUserProvider extends OneRosterUserProvider {
    constructor(app) {
        super(app)
    }
    async preSync(context, filter) {
        const results = await Promise.sequence([
            () => context.model('CourseClassInstructors').select(
                        'instructor/user/name as username',
                        'instructor/user/enabled as enabledUser',
                        'instructor/user/id as identifier',
                        'instructor/email as email',
                        'instructor/familyName as familyName',
                        'instructor/givenName as givenName',
                        'instructor/middleName as middleName',
                        'instructor/workPhone as phone',
                        'instructor/department/id as department',
                        'instructor/department/organization as organization',
                        'instructor/user/dateModified as dateLastModified',
                        'courseClass/course/provider as provider'
                    ).where('instructor/user').notEqual(null)
                        .and('courseClass/year').equal(filter.academicYear).and('courseClass/period').equal(filter.academicPeriod)
                        .and('courseClass/course/provider').notEqual(null)
                        .silent().take(-1).getItems(),
            () => context.model('StudentCourseClass').select(
                        'student/user/name as username',
                        'student/user/id as identifier',
                        'student/user/enabled as enabledUser',
                        'student/person/email as email',
                        'student/person/familyName as familyName',
                        'student/person/givenName as givenName',
                        'student/homeInstitute/id as homeInstitute',
                        'student/user/dateModified as dateLastModified',
                        'courseClass/course/provider as provider',
                        'student/department/organization as organization',
                    ).where('student/user').notEqual(null)
                        .and('courseClass/year').equal(filter.academicYear).and('courseClass/period').equal(filter.academicPeriod)
                        .and('courseClass/course/provider').notEqual(null)
                        .silent().take(-1).getItems(),
            () => context.model('OneRosterOrg').silent().take(-1).getItems()
        ]);
        const chance = new Chance();
        // append instructors as users
        const users = [];
        const orgs = results[2];
        results[0].forEach((instructor) => {
            const findUser = users.find((x) => x.username === instructor.username);
            if (findUser && instructor.provider) {
                const findIndex = findUser.orgs.findIndex((org) => org.identifier === `INST-${instructor.provider}`);
                if (findIndex < 0) {
                    // findUser.orgs.push({
                    //     identifier: `INST-${instructor.provider}`
                    // });
                }
                return;
            }
            let findInstructorOrg = null;
            if (instructor.provider) {
                findInstructorOrg = orgs.find((org) => org.identifier === `INST-${instructor.provider}`);
            }
            const res = {
                role: 'teacher',
                status: instructor.enabledUser ? 'active' : 'inactive',
                dateLastModified: instructor.dateLastModified,
                username: instructor.username,
                identifier: instructor.username,
                enabledUser: instructor.enabledUser,
                email: instructor.email,
                familyName: instructor.familyName,
                middleName: instructor.middleName,
                givenName: instructor.givenName,
                password: chance.string({
                    length: 12,
                    alpha: true,
                    numeric: true,
                    symbols: true
                }),
                orgs: []
            };
            if (findInstructorOrg) {
                // Object.assign(res, {
                //     orgs: [
                //         {
                //             identifier: findInstructorOrg.identifier
                //         }
                //     ]
                // })
            }
            users.push(res);
        });
        // append students as users
        results[1].forEach((student) => {
            const findUser = users.find((x) => x.username === student.username);
            if (findUser && student.provider) {
                const findIndex = findUser.orgs.findIndex((org) => org.identifier === `INST-${student.provider}`);
                if (findIndex < 0) {
                    // findUser.orgs.push({
                    //     identifier: `INST-${student.provider}`
                    // });
                }
                return;
            }
            let findStudentOrg = null;
            if (student.provider) {
                findStudentOrg = orgs.find((org) => org.identifier === `INST-${student.provider}`);
            }
            const res = {
                role: 'student',
                status: student.enabledUser ? 'active' : 'inactive',
                dateLastModified: student.dateLastModified,
                username: student.username,
                identifier: student.username,
                enabledUser: student.enabledUser,
                email: student.email,
                familyName: student.familyName,
                givenName: student.givenName,
                password: chance.string({
                    length: 12,
                    alpha: true,
                    numeric: true,
                    symbols: true
                }),
                orgs: []
            };
            if (findStudentOrg) {
                // Object.assign(res, {
                //     orgs: [
                //         {
                //             identifier: findStudentOrg.identifier
                //         }
                //     ]
                // })
            }
            users.push(res);
        });
        return [
            [
                'OneRosterUser',
                users
            ]
        ]
    }
}

export {
    EpicurOneRosterUserProvider
}