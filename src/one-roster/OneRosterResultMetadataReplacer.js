import path from 'path';
import { ApplicationService } from '@themost/common';
import { SchemaLoaderStrategy } from '@themost/data';

class OneRosterResultMetadataReplacer extends ApplicationService {
    constructor(app) {
        super(app);
    }

    apply() {
        // get schema loader
        const schemaLoader = this.getApplication().getConfiguration().getStrategy(SchemaLoaderStrategy);
        /**
         * @type {import('@themost/common').DataModelProperties}
         */
        const model = schemaLoader.getModelDefinition('OneRosterResultMetadata');
        model.eventListeners = model.eventListeners || [];
        const type = path.resolve(__dirname, 'OnResultMetadataUpdate');
        const found = model.eventListeners.find((item) => item.type === type);
        if (found == null) {
            model.eventListeners.push({
                type
            });
        }
        
    }

}

export {
    OneRosterResultMetadataReplacer
}