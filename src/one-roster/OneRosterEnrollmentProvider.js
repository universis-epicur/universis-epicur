import { OneRosterEnrollmentProvider, OneRosterAcademicSession }  from '@universis/one-roster';
import moment from 'moment';
class EpicurOneRosterEnrollmentProvider extends OneRosterEnrollmentProvider {
    constructor(app) {
        super(app)
    }
    async preSync(context, filter) {
        const results = await Promise.sequence([
            () => context.model('CourseClassInstructors').select(
                        'courseClass/id as classCode',
                        'courseClass/course/department/id as classDepartment',
                        'courseClass/course/provider as provider',
                        'courseClass/year/id as academicYear',
                        'courseClass/period/id as academicPeriod',
                        'instructor/user/name as username',
                        'dateModified as dateLastModified',
                        'instructor/department/organization as organization'
                    ).where('instructor/user').notEqual(null)
                        .and('courseClass/year').equal(filter.academicYear).and('courseClass/period').equal(filter.academicPeriod)
                        .and('courseClass/course/provider').notEqual(null)
                        .silent().take(-1).getItems(),
            () => context.model('StudentCourseClass').select(
                        'courseClass/id as classCode',
                        'courseClass/course/department/id as classDepartment',
                        'courseClass/course/provider as provider',
                        'student/user/name as username',
                        'dateModified as dateLastModified',
                        'student/homeInstitute as organization',
                        'courseClass/startDate as startDate',
                        'courseClass/endDate as endDate',
                        'courseClass/year/id as academicYear',
                        'courseClass/period/id as academicPeriod'
                    ).where('student/user').notEqual(null)
                    .and('courseClass/year').equal(filter.academicYear).and('courseClass/period').equal(filter.academicPeriod)
                    .and('courseClass/course/provider').notEqual(null)
                    .silent().take(-1).getItems(),
            () => context.model('OneRosterOrg').silent().take(-1).getItems()
        ]);

        const instructorEnrollments = [];

        results[0].forEach((instructor) => {
            const find = instructorEnrollments.find((x) => 
                x.user.username === instructor.username &&
                x.class.classCode === instructor.classCode &&
                x.school.identifier === `INST-${instructor.provider}`);
            if (find) {
                return;
            }
            instructorEnrollments.push({
                user: {
                    username: instructor.username
                },
                class: {
                    classCode: instructor.classCode
                },
                school: {
                    identifier: `INST-${instructor.provider}`
                },
                role: 'teacher',
                primary: true,
                beginDate: null,
                endDate: null
            });
        });

        const studentEnrollments = [];

        results[1].forEach((student) => {
            const term = OneRosterAcademicSession.generateId(student.academicYear, student.academicPeriod);
            const find = studentEnrollments.find((x) => 
                x.user.username === student.username &&
                x.class.classCode === student.classCode &&
                x.school.identifier === `INST-${student.provider}`);
            if (find) {
                return;
            }
            studentEnrollments.push({
                user: {
                    username: student.username
                },
                class: {
                    classCode: student.classCode
                },
                school: {
                    identifier: `INST-${student.provider}`
                },
                role: 'student',
                primary: false,
                beginDate: null,
                endDate: null,
                metadata: {
                    term
                }
            });
        });
        const enrollments = [].concat(instructorEnrollments, studentEnrollments);
        return [
            [
                'OneRosterEnrollment',
                enrollments
            ]
        ]

    }
}

export {
    EpicurOneRosterEnrollmentProvider
}