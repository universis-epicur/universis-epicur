import { ApplicationBase } from '@themost/common';
import { OneRosterService } from '@universis/one-roster';

export declare class EpicurOneRosterService extends OneRosterService {
    constructor(app: ApplicationBase);
}