/**
 * @param {import('@themost/data').DataEventArgs} event
 */
 async function beforeSaveAsync(event) {
    if (Object.prototype.hasOwnProperty.call(event.target, 'result')) {
        const context = event.model.context;
        // get username
        const {username, score} = await context.model('OneRosterResult').silent().find(event.target.result).select(
            'student/username as username',
            'score'
            ).getItem();
        if (username != null && typeof score === 'number') {
            // get home institution grading scheme
            const  instituteGradeScale = await context.model('Student').asQueryable().where('user/name').equal(username)
                .select('homeInstitute/gradeScale').silent().value();
            if (instituteGradeScale) {
                const gradeScale = await context.model('GradeScale').silent().find(instituteGradeScale).getTypedItem();
                if (gradeScale) {
                    event.target.altScoreText = gradeScale.convertTo(score/100);
                }
            } 
        }
    }
 }
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return beforeSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}