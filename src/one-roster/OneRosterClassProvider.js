import { OneRosterClassProvider, OneRosterAcademicSession }  from '@universis/one-roster';

class EpicurOneRosterClassProvider extends OneRosterClassProvider {
    constructor(app) {
        super(app)
    }
    async preSync(context, filter) {

        const results = await Promise.sequence([
            () => context.model('CourseClass').select(
                'id as classCode',
                'title as title',
                'year/id as academicYear',
                'period/id as academicPeriod',
                'course/id as course',
                'course/department/id as department',
                'course/provider as provider',
                'status/alternateName as status',
                'dateModified'
                ).where('year').equal(filter.academicYear).and('period').equal(filter.academicPeriod)
                .and('course/provider').notEqual(null)
                .silent().take(-1).getItems()
        ]);

        const classes = results[0].map((item) => {
            return {
                status: item.status === 'open' || item.status === 'closed' ? 'active' : 'inactive',
                dateLastModified: item.dateModified || new Date(),
                type: 'scheduled',
                title: item.title,
                classCode: item.classCode,
                course: {
                    courseCode: item.course
                },
                school: {
                    identifier: `INST-${item.provider}`
                },
                terms: [
                    {
                        sourcedId: OneRosterAcademicSession.generateId(item.academicYear, item.academicPeriod)
                    }
                ]
            }
        });

        return [
            [
                'OneRosterClass',
                classes
            ]
        ];
    }
}

export {
    EpicurOneRosterClassProvider
}