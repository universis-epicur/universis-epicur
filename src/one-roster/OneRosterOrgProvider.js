import { OneRosterOrgProvider }  from '@universis/one-roster';

class EpicurOneRosterOrgProvider extends OneRosterOrgProvider {
    constructor(app) {
        super(app)
    }
    /**
     * @param {DataContext} context 
     * @param {OneRosterProviderFilter} filter
     */
     async preSync(context, filter) {
        const results = await Promise.sequence([
            () => context.model('Institute').where('partner').equal(true).take(-1).silent().getItems(),
            () => context.model('Institute').where('local').equal(true).take(-1).silent().getItems()
        ]);
        const partners = results[0].map((item) => {
            return {
                status: 'active',
                dateLastModified: new Date(),
                name: item.name,
                type: 'school',
                identifier: `INST-${item.id}`,
                parent: null
            }
        });
        const institutes = results[1].map((item) => {
            return {
                status: 'active',
                dateLastModified: new Date(),
                name: item.name,
                type: 'school',
                identifier: `INST-${item.id}`,
                parent: null
            }
        });
        const orgs = [].concat(partners, institutes);
        return [
            [
                'OneRosterOrg',
                orgs
            ]
        ];
    }
}

export {
    EpicurOneRosterOrgProvider
}