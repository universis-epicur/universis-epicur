import { OneRosterCourseProvider }  from '@universis/one-roster';

class EpicurOneRosterCourseProvider extends OneRosterCourseProvider {
    constructor(app) {
        super(app)
    }
    async preSync(context, filter) {
        const items = await context.model('CourseClass').select(
            'course/id as courseCode',
            'course/name as title',
            'course/department/id as department',
            'course/isEnabled as enabled',
            'course/dateModified as dateModified',
            'course/provider as provider'
        ).where('year').equal(filter.academicYear).and('period').equal(filter.academicPeriod)
            .and('course/provider').notEqual(null)
            .silent().take(-1).getItems();

        const courses = items.map((item) => {
            return {
                status: item.enabled ? 'active' : 'inactive',
                dateLastModified: item.dateModified || new Date(),
                title: item.title,
                courseCode: item.courseCode,
                org: {
                    identifier: `INST-${item.provider}`
                }
            }
        });
        return [
            [
                'OneRosterCourse',
                courses
            ]
        ];
    }
}

export {
    EpicurOneRosterCourseProvider
}