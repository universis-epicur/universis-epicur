import {FileSchemaLoaderStrategy} from "@themost/data";
import path from 'path';
/**
 * A schema loader for loading data models associated with document numbering services
 */
export class EpicurSchemaReplacer extends FileSchemaLoaderStrategy {

    /**
     * @param {ConfigurationBase} config
     */
    constructor(config) {
        super(config);
        this.setModelPath(path.resolve(__dirname, 'config/replace/models'));
    }

    getModelDefinition(name) {
        return super.getModelDefinition.bind(this)(name);
    }

}

