import {AttachmentFileSystemStorage} from '@themost/web';
import { HttpNotFoundError, HttpServerError } from '@themost/common';
import path from 'path';
import express from 'express';


class PublicContentService extends AttachmentFileSystemStorage {
    /**
     * @param {IApplication} app
     */
    constructor(app) {
        // get physical path from application configuration or use content/private as default path
        super(path.resolve( app.getConfiguration().getSourceAt('settings/universis/content/root') || 'content/private'));
        // set virtual path
        this.virtualPath = '/api/content/public/';
        const thisService = this;
        if (app) {
            app.serviceRouter.subscribe((serviceRouter) => {
                if (serviceRouter) {
                    // create a router
                    const newRouter = express.Router();
                    newRouter.get('/content/public/:file', (req, res, next) => {
                        try {
                            thisService.findOne(req.context, {
                                additionalType: 'PublicAttachment',
                                alternateName: req.params.file
                            }, function(err, result) {
                                if (err) {
                                    TraceUtils.error(err);
                                    return next(new HttpServerError());
                                }
                                if (result==null) {
                                    return next(new HttpNotFoundError());
                                }
                                thisService.resolvePhysicalPath(req.context, result, function(err, executionPath) {
                                    if (err) {
                                        return next(err);
                                    }
                                return res.sendFile(executionPath, {
                                    headers: {
                                        'Content-Disposition': `inline; filename="${result.alternateName}"`,
                                        'Cache-Control': 'public, max-age=36000',
                                        'Content-Type': result.contentType
                                    }
                                },  (err) => {
                                    if (err) {
                                        if (err.code === 'ENOENT') {
                                            return next(new HttpNotFoundError());
                                        }
                                        return next(err);
                                    }
                                });
                                });
                            });
                        } catch (error) {
                            return next(error);
                        }
                    });
                    // insert router at the beginning of serviceRouter.stack
                    serviceRouter.stack.unshift.apply(serviceRouter.stack, newRouter.stack);
                }
            });
        }

    }
}

export {
    PublicContentService
}