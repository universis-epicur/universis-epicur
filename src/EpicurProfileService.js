import { ProfileService } from '@universis/profiles';
import { applicationConfigurationRouter } from './applicationConfigurationRouter';
export class EpicurProfileService extends ProfileService {
    constructor(app) {
        super(app);

        if (app && app.container) {
            app.container.subscribe((container) => {
                if (container) {
                    container.use('/.well-known/application-configuration', applicationConfigurationRouter(app));
                }
            });
        }

    }
    getStudentProfiles(context) {
        return context.model('Student')
            .where('user/name').equal(context.user.name)
            .and('user/name').notEqual(null)
            .select('id',
                'studyProgram/name as title',
                'studyProgramSpecialty/name as specializationName',
                'studyProgramSpecialty/specialty as specializationIndex',
                'studyProgram/department/name as description',
                'inscriptionYear/name as inscriptionYear',
                'inscriptionPeriod/name as inscriptionPeriod'
            )
            .orderByDescending('inscriptionYear', 'inscriptionPeriod')
            .silent()
            .getItems().then((items) => {
                return items.map((item) => {
                    return {
                        ...item,
                        title: (Number.isInteger(item.specializationIndex) && item.specializationIndex > -1) ? `${item.title} (${item.specializationName})` : item.title,
                        description: `${item.description} ${item.inscriptionYear} ${item.inscriptionPeriod}`
                    }
                });
            });
    }
}