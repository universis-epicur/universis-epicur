import { KeycloakClientService } from '@universis/keycloak';
import { TraceUtils } from '@themost/common';
import { ScopeString } from '@universis/janitor';
export class EpicurKeycloakClientService extends KeycloakClientService {
    constructor(app) {
        super(app);
    }

    /**
     * Gets token info by calling keycloak server endpoint
     * Override method to validate user and create a new one if he/she is a candidate student
     * @param {*} context
     * @param {string} token
     */
    getTokenInfo(context, token) {
        return super.getTokenInfo(context, token).then(
            /**
             * @param {{active: boolean, scope: string, username: string}} result;
             */
            (result) => {
                // if token info exists and is active
                if (result && result.active) {
                    // validate token that has students scope
                    const scopes = new ScopeString(result.scope).split();
                    const Users = context.model('User');
                    // get local username attribute which will be either account email or username
                    const localUsername = result.email || result.username;
                    if (localUsername == null) {
                        return Promise.resolve(result);
                    }
                    if (scopes.indexOf('students') >= 0) {
                        // get user (either by name or alternateName)
                        return Users.where('name').equal(localUsername)
                            .or('alternateName').equal(localUsername)
                            .silent().count()
                            .then((exists) => {
                                // if user already exists
                                // do nothing and return token info
                                if (exists) {
                                    return Promise.resolve(result);
                                }
                                // otherwise try to create a user and add it to candidates group
                                let description = null;
                                if (result.given_name != null && result.family_name != null) {
                                    description = `${result.given_name} ${result.family_name}`;
                                }
                                return Users.silent().save({
                                    name: result.username,
                                    alternateName: result.email,
                                    description: description,
                                    enabled: true,
                                    groups: [
                                        {
                                            name: 'Candidates'
                                        }
                                    ]
                                }).then(() => {
                                    // a new user has been created
                                    // so return token info
                                    return Promise.resolve(result);
                                });
                            });
                    } else if (scopes.indexOf('teachers') >= 0) {
                        if (localUsername == null) {
                            return Promise.resolve(result);
                        }
                        // try to find user (either by name or alternateName)
                        return Users.where('name').equal(localUsername)
                            .or('alternateName').equal(localUsername)
                            .silent().count()
                            .then((exists) => {
                                // if user already exists
                                // do nothing and return token info
                                if (exists) {
                                    return Promise.resolve(result);
                                }
                                // search for instructors with the same email
                                const Instructors = context.model('Instructor');
                                return Instructors.where('email').equal(result.email).silent().getItem().then((instructor) => {
                                    if (instructor == null) {
                                        return Promise.resolve(result);
                                    }
                                    // otherwise save user
                                    const newUser = {
                                        name: result.username,
                                        alternateName: result.email,
                                        description: `${instructor.givenName} ${instructor.familyName}`,
                                        enabled: true,
                                        groups: [
                                            {
                                                name: 'Instructors'
                                            }
                                        ]
                                    };
                                    return Users.on('after.save', (event, callback) => {
                                        if (event.state === 1) {
                                            instructor.user = event.target;
                                            return Instructors.silent().save(instructor).then(() => {
                                                return callback();
                                            }).catch((err) => {
                                                return callback(err);
                                            });
                                        }
                                        return callback();
                                    }).silent().save(newUser).then(() => {
                                        // a new user has been created
                                        // so return token info
                                        return Promise.resolve(result);
                                    });
                                });
                            });
                    }
                }
                return Promise.resolve(result);
            }).then((result) => {
                const Users = context.model('User');
                // this operation will try to update local user.name and user.alternateName
                // based on the keycloak username and email
                // user.name <= <keycloak account>.username
                // user.alternateName <= <keycloak account>.email
                const localUsername = result.email || result.username;
                if (localUsername == null) {
                    return Promise.resolve(result);
                }
                return Users.where('name').equal(localUsername)
                    .or('alternateName').equal(localUsername)
                    .select('id', 'name', 'alternateName', 'enabled')
                    .silent()
                    .getItem().then((user) => {
                        if (user == null) {
                            return Promise.resolve(result);
                        }
                        // if user should be update due to changes
                        if (user.name !== result.username || user.alternateName !== result.email) {
                            TraceUtils.debug(`User name has been changed. ${user.name} is going to be updated`);
                            // force update user
                            return Users.silent().save(Object.assign(user, {
                                name: result.username,
                                alternateName: result.email
                            })).then(() => {
                                // start an async operation of synchronizing current registration
                                // this operation is going to be removed in the future
                                const asyncContext = context.getApplication().createContext();
                                (async function syncLastRegistration(currentUser) {
                                    const student = await asyncContext.model('Students')
                                        .where('user/name').equal(currentUser.name)
                                        .expand('department')
                                        .silent().getTypedItem();
                                    if (student == null) {
                                        // do nothing and exit
                                        return;
                                    }
                                    const currentRegistration = await asyncContext.model('StudentPeriodRegistration')
                                        .where("student").equal(student.id)
                                        .and("registrationYear").equal(student.department.currentYear)
                                        .and("registrationPeriod").equal(student.department.currentPeriod)
                                        .silent().getTypedItem();
                                    if (currentRegistration != null) {
                                        TraceUtils.debug(`Current registration of ${result.username} is going to be synchronized`);
                                        // sync current registration
                                        await currentRegistration.sync();
                                    } else {
                                        TraceUtils.debug(`Current registration of ${result.username} is empty.`);
                                    }
                                })(user).then(() => {
                                    asyncContext.finalize();
                                }).catch((err) => {
                                    TraceUtils.error('Student', 'An error occurred while synchronizing last student registration.');
                                    TraceUtils.error(err);
                                });
                                // exit
                                return Promise.resolve(result);
                            });
                        }
                        return Promise.resolve(result);
                    });
            });
    }

}