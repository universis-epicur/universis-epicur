import { getMailer } from '@themost/mailer';
/**
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {
    if (event.state === 1) {
        const context = event.model.context;
        const recipient = await context.model('Account').find(event.target.recipient).silent().getItem();
        if (recipient == null) {
            return;
        }
        /**
         * @type {import('@themost/mailer').MailerHelper}
         */
        const mailer = getMailer(context);
        // get parent action data
        const item = await context.model('StudyProgramRegisterAction')
            .where('id').equal(event.target.action)
            .expand('candidate', 'owner')
            .silent().getItem();
        if (item) {
            if (recipient.id !== item.owner.id) {
                return;
            }
            await new Promise((resolve, reject) => {
                mailer.template('new-message')
                    .subject('EPICUR - New message')
                    .to(item.owner.name)
                    .send({
                        model: item
                    }, (err) => {
                        if (err) {
                            return reject(err);
                        }
                        return resolve();
                    })
            });

        }
    }
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}