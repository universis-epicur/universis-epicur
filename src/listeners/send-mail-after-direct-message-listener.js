import { getMailer } from '@themost/mailer';
/**
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {
    if (event.state === 1) {
        const context = event.model.context;
        /**
         * @type {import('@themost/mailer').MailerHelper}
         */
        const mailer = getMailer(context);
        // get parent action data
        const item = await event.model
            .where('id').equal(event.target.id)
            .expand('candidateStudent')
            .silent().getItem();
        if (item) {
            await new Promise((resolve, reject) => {
                mailer.template('direct-message')
                    .subject(item.subject)
                    .to(item.candidateStudent.email)
                    .send({
                        model: item
                    }, (err) => {
                        if (err) {
                            return reject(err);
                        }
                        return resolve();
                    })
            });

        }
    }
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}