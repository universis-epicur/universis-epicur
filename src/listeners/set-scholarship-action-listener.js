
/**
 * @param {DataEventArgs} event
 */
 async function afterSaveAsync(event) {
    const context = event.model.context;
    // get original data
    const actionStatus = await context.model('StudyProgramRegisterAction')
        .where('id').equal(event.target.id).select('actionStatus/alternateName').value();
    // get previous status
    let previousStatus = 'UnknownActionStatus';
    if (event.previous) {
        previousStatus = event.previous.actionStatus.alternateName;
    }
    if (actionStatus === 'ActiveActionStatus' && previousStatus === 'PotentialActionStatus') {
        // get scholarship action if any
        const model = context.model('RequestEnrollmentScholarshipAction');
        if (model != null) {
            const scholarshipAction = await model.where('initiator').equal(event.target.id).getItem();
            if (scholarshipAction && scholarshipAction.actionStatus && scholarshipAction.actionStatus.alternateName === 'PotentialActionStatus') {
                const {id} = scholarshipAction;
                const actionStatus = {
                    alternateName: 'ActiveActionStatus'
                }
                await model.silent().save({
                    id,
                    actionStatus
                });
            }
        }
    }
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}