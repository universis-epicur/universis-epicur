import { DataError } from '@themost/data';
/**
 * @param {DataEventArgs} event
 */
async function beforeSaveAsync(event) {
    const context = event.model.context;
    let actionStatus;
    if (Object.prototype.hasOwnProperty.call(event.target, 'actionStatus') === false) {
        return;
    }
    // find current action status
    actionStatus = await context.model('ActionStatusTypes')
        .find(event.target.actionStatus)
        .select('alternateName').value();
    if (event.state === 2) {
        // get previous status
        let previousStatus = 'UnknownActionStatus';
        if (event.previous) {
            previousStatus = event.previous.actionStatus.alternateName;
        } else {
            throw new DataError('E_STATE', 'The previous state of an object cannot be determined', null, 'StudyProgramRegisterAction');
        }
        if (previousStatus === 'PotentialActionStatus' && actionStatus === 'ActiveActionStatus') {
            event.target.endTime = new Date();
        }
    } else if (event.state === 1) {
        if (actionStatus === 'ActiveActionStatus') {
            event.target.endTime = new Date();
        }
    }
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return beforeSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}