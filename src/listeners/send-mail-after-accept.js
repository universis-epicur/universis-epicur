import { getMailer } from '@themost/mailer';
import { DataError } from '@themost/data';
import moment from 'moment';
/**
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {
    if (event.state === 2) {
        const context = event.model.context;
        /**
         * @type {import('@themost/mailer').MailerHelper}
         */
        const mailer = getMailer(context);

        const actionStatus = await context.model('StudyProgramRegisterAction')
            .where('id').equal(event.target.id).select('actionStatus/alternateName').value();
        let previousStatus = 'UnknownActionStatus';
        if (event.previous) {
            previousStatus = event.previous.actionStatus.alternateName;
        } else {
            throw new DataError('E_STATE', 'The previous state of an object cannot be determined', null, 'StudyProgramRegisterAction');
        }
        if (actionStatus === previousStatus) {
            return;
        }
        if (actionStatus == 'CompletedActionStatus') {
            const item = await context.model('StudyProgramRegisterAction')
                .where('id').equal(event.target.id)
                .expand(
                    'candidate', 'owner', 'studyProgram', {
                    name: 'courseRegistrations',
                    options: {
                        $filter: 'actionStatus/alternateName eq \'CompletedActionStatus\'',
                        $expand: 'courseClass($expand=participationType,course($expand=provider))'
                    }
                }
                )
                .levels(3)
                .silent().getItem();
            if (item) {
                if (item.courseRegistrations.length === 0) {
                    return;
                }
                // get specialization course
                const specializationCourses = await context.model('SpecializationCourse')
                    .where('specialization').equal(item.specialization)
                    .select(
                        'courseType/name as courseType',
                        'studyProgramCourse/course as course',
                        'ects'
                    ).getAllItems();
                
                item.courseRegistrations.forEach((courseRegistration) => {
                    const specializationCourse = specializationCourses.find((x) => {
                        return x.course === courseRegistration.courseClass.course.id;
                    });
                    if (specializationCourse) {
                        const {courseType, ects} = specializationCourse;
                        Object.assign(courseRegistration, {
                            courseType,
                            ects
                        });
                    }
                });

                await new Promise((resolve, reject) => {
                    mailer.template('application-accepted')
                        .subject('EPICUR - Your application has been accepted')
                        .to(item.owner.name)
                        .send({
                            model: item,
                            html: {
                                moment
                            }
                        }, (err) => {
                            if (err) {
                                return reject(err);
                            }
                            return resolve();
                        })
                });

            }
        }
    }
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}