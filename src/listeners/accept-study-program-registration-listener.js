import { DataObjectState, FunctionContext, DataCacheStrategy } from "@themost/data";
import { DataError, AccessDeniedError, DataNotFoundError } from "@themost/common";
import { promisify } from 'es6-promisify';
/**
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {

    const context = event.model.context;
    // get current status
    const actionStatus = await context.model('StudyProgramRegisterAction')
        .where('id').equal(event.target.id).select('actionStatus/alternateName').value();

    if (actionStatus !== 'CompletedActionStatus') {
        return;
    }
    if (event.state === DataObjectState.Insert) {
        throw new AccessDeniedError('A study program registration cannot be completed automatically.', null);
    }
    if (event.state === DataObjectState.Update) {
        // get previous status
        let previousStatus = 'UnknownActionStatus';
        if (event.previous) {
            previousStatus = event.previous.actionStatus.alternateName;
        } else {
            throw new DataError('E_STATE', 'The previous state of an object cannot be determined', null, 'StudyProgramRegisterAction');
        }
        if (previousStatus === 'ActiveActionStatus') {
            // create student
            /**
             * @type {StudyProgramRegisterAction}
             */
            const item = await context.model('StudyProgramRegisterAction').where('id').equal(event.target.id)
                .expand('studyProgram', 'actionStatus', {
                    name: 'courseRegistrations',
                    options: {
                        $expand: 'courseClass'
                    }
                },
                    {
                        name: 'internshipRegistrations',
                        options: {
                            $expand: 'actionStatus'
                        }
                    }
                    , {
                        name: 'candidate',
                        options: {
                            $expand: 'currentInstitute'
                        }
                    }).levels(3).silent().getItem();
            if (item == null) {
                throw new DataNotFoundError('Current item cannot be found or is inaccessible', null, 'StudyProgramRegisterAction');
            }
            // get study program data
            let studyProgramSpecialty = item.specialization;
            if (item.specialization == null) {
                // get study program core specialization
                studyProgramSpecialty = await context.model('StudyProgramSpecialty')
                    .where('studyProgram')
                    .equal(item.studyProgram.id)
                    .and('specialty').equal(-1)
                    .getItem();
            }

            // try to find Person
            let person = await context.model('Person').where('email').equal(item.candidate.email).silent().getItem();
            if (person == null) {
                // create person
                person = Object.keys(item.candidate).reduce((previous, key) => {
                    const value = item.candidate[key];
                    if (value != null) {
                        Object.assign(previous, {
                            [key]: value
                        });
                    }
                    return previous;
                }, {});
                // remove candidate id in order to insert a person based on its data
                delete person.id;
                await context.model('Person').save(person);
            }
            // create student
            // important note: each student has a unique for each study program
            let newStudent = {
                person: person,
                department: item.studyProgram.department,
                studentStatus: {
                    alternateName: 'active'
                },
                studyProgram: item.studyProgram,
                studyProgramSpecialty: studyProgramSpecialty,
                semester: 1,
                inscriptionMode: {
                    name: 'Mobility'
                },
                inscriptionYear: item.inscriptionYear,
                inscriptionPeriod: item.inscriptionPeriod,
                inscriptionDepartment: item.candidate.currentInstitute && item.candidate.currentInstitute.alternateName,
                inscriptionDate: new Date(),
                inscriptionSemester: 1,
                homeInstitute: item.candidate.currentInstitute,
                user: item.owner
            };
            const Students = context.model('Student');
            const studentIdentifier = await new FunctionContext(context, Students).newStudentIdentifier(newStudent);
            Object.assign(newStudent, {
                studentIdentifier: studentIdentifier
            });
            const student = await Students.save(newStudent);
            // add user to students group
            const userGroups = context.model('User').convert(item.owner).property('groups');
            // add to students group
            await userGroups.silent().insert({
                name: 'Students'
            });
            // in some cases a candidate may register for courses
            // which are going to be taught in more than one academic periods
            // so we should create multiple student registrations
            const newRegistrationPeriods = [
                {
                    registrationYear: item.inscriptionYear,
                    registrationPeriod: item.inscriptionPeriod
                }
            ];
            const AcademicYears = context.model('AcademicYear');
            const AcademicPeriods = context.model('AcademicPeriod');
            item.courseRegistrations.forEach((item) => {

                const courseClassYear = AcademicYears.convert(item.courseClass.year).getId();
                const courseClassPeriod = AcademicPeriods.convert(item.courseClass.period).getId();

                const findPeriod = newRegistrationPeriods.find((x) => {
                    return x.registrationYear === courseClassYear &&
                        x.registrationPeriod === courseClassPeriod
                });
                if (findPeriod == null) {
                    newRegistrationPeriods.push({
                        registrationYear: courseClassYear,
                        registrationPeriod: courseClassPeriod
                    });
                }
            });

            // eslint-disable-next-line no-unused-vars
            for (const newRegistrationPeriod of newRegistrationPeriods) {
                // create registration
                let newRegistration = {
                    student: student,
                    registrationYear: newRegistrationPeriod.registrationYear,
                    registrationPeriod: newRegistrationPeriod.registrationPeriod,
                    classes: item.courseRegistrations.filter((courseRegistration) => {
                        return courseRegistration.actionStatus.alternateName === 'CompletedActionStatus';
                    }).filter((courseRegistration) => {
                        return AcademicYears.convert(courseRegistration.courseClass.year).getId() === newRegistrationPeriod.registrationYear &&
                            AcademicPeriods.convert(courseRegistration.courseClass.period).getId() === newRegistrationPeriod.registrationPeriod;
                    }).map((courseRegistration) => {
                        return {
                            courseClass: courseRegistration.courseClass
                        };
                    })
                }
                let currentRegistration = context.model('StudentPeriodRegistration').convert(newRegistration);
                let saveAsync = promisify(currentRegistration.save.bind(currentRegistration));
                await saveAsync(context);
                if (currentRegistration.validationResult && currentRegistration.validationResult.success === false) {
                    throw currentRegistration.validationResult;
                }
                // get student registration and close it
                await context.model('StudentPeriodRegistration').save({
                    id: currentRegistration.id,
                    status: {
                        alternateName: 'closed'
                    },
                    student: student,
                    registrationYear: newRegistrationPeriod.registrationYear,
                    registrationPeriod: newRegistrationPeriod.registrationPeriod
                });
            }
            // validate internship registration
            if (Array.isArray(item.internshipRegistrations)) {
                const internshipRegistration = item.internshipRegistrations.find((x) => {
                    return x.actionStatus && x.actionStatus.alternateName === 'CompletedActionStatus';
                });
                if (internshipRegistration) {
                    // get intership
                    const internship = await context.model('Internship').where('id').equal(internshipRegistration.internship)
                        .expand('status')
                        .silent().getItem();
                    if (internship == null) {
                        throw new DataError('E_INTERNSHIP_NOENT', 'The specified internship cannot be found or is inaccessible', null, 'InternshipRegisterAction', 'internship');
                    }
                    if (internship.student != null) {
                        throw new DataError('E_INTERNSHIP_STUDENT', 'Target internship has a wrong state. Internship has been already assigned to a student.', null, 'Internship', 'student');
                    }
                    if (internship.status.alternateName != 'potential') {
                        throw new DataError('E_INTERNSHIP_STATUS', 'Target internship is not available.', null, 'Internship', 'status');
                    }
                    Object.assign(internship, {
                        student: student,
                        internshipDate: new Date(),
                        status: {
                            alternateName: 'active'
                        }
                    });
                    // and finally update
                    await context.model('Internship').silent().save(internship);
                }
            }


            const cache = context.getApplication().getConfiguration().getStrategy(DataCacheStrategy);
            if (cache != null) {
                cache.clear();
            }

        }
    }
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}
