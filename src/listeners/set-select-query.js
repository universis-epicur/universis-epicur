import fs from 'fs';
import path from 'path';
export function afterUpgrade(event, callback) {
    const sqlFile = path.resolve(__dirname, `../scripts/${event.model.name}.sql`);
    return fs.stat(sqlFile, (err, stats) => {
        if (err) {
            return callback(err);
        }
        if (stats.isFile() === false) {
            return callback();
        }
        return fs.readFile(sqlFile, (err, data) => {
            if (err) {
                return callback(err);
            }
            const sql = data.toString();
            event.model.context.db.execute(sql, [], (err) => {
                if (err) {
                    return callback(err);
                }
                return callback();
            });
        });
    });
}