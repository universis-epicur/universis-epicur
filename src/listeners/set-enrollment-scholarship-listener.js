
/**
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {
    const context = event.model.context;
    if (Object.prototype.hasOwnProperty.call(event.target, 'scholarships')) {
        const { id, scholarships } = event.target;
        scholarships.forEach((item) => {
            item.initiator = {
                id
            };
        });
        // validate scholarships
        await context.model('RequestEnrollmentScholarshipAction').save(scholarships);
    }
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}