import { DataError, DataNotFoundError, AccessDeniedError } from '@themost/common';
import { DataPermissionEventListener, PermissionMask } from '@themost/data';

function validateEnrollmentPeriod(enrollmentPeriod) {
    const now = new Date();
    let valid = false;
    if (enrollmentPeriod.validFrom instanceof Date) {
        if (enrollmentPeriod.validThrough instanceof Date) {
            valid = enrollmentPeriod.validFrom <= now && enrollmentPeriod.validThrough > now;
        } else {
            valid = enrollmentPeriod.validFrom < now;
        }
    } else if (enrollmentPeriod.validThrough instanceof Date) {
        valid = enrollmentPeriod.validThrough > now;
    }
    return valid;
}
/**
 * @param {DataEventArgs} event
 */
async function beforeSaveAsync(event) {
    // check if current user is the owner of this object
    let owner;
    let enrollmentPeriod;
    const context = event.model.context;
    if (event.state === 2) {
        if (Object.prototype.hasOwnProperty.call(event.target, 'owner')) {
            // get owner
            owner = await context.model('User').find(event.target.owner).select('name').silent().value();
        } else {
            owner = await context.model('User').find(event.previous.owner).select('name').silent().value();
        }
        enrollmentPeriod = await context.model('StudyProgramEnrollmentEvent')
            .where('studyProgram').equal(event.previous.studyProgram)
            .and('inscriptionYear').equal(event.previous.inscriptionYear)
            .and('inscriptionPeriod').equal(event.previous.inscriptionPeriod)
            .and('eventStatus/alternateName').equal('EventOpened')
            .silent()
            .getItem();
    } else if (event.state === 1) {
        // get owner
        owner = await context.model('User').find(event.target.owner).select('name').silent().value();
        enrollmentPeriod = await context.model('StudyProgramEnrollmentEvent')
            .where('studyProgram').equal(event.target.studyProgram)
            .and('inscriptionYear').equal(event.target.inscriptionYear)
            .and('inscriptionPeriod').equal(event.target.inscriptionPeriod)
            .and('eventStatus/alternateName').equal('EventOpened')
            .silent()
            .getItem();
    }
    if (owner == null) {
        throw new DataNotFoundError('Action owner cannot be found or is inaccessible', null, event.model.name, event.target.id);
    }
    // check if current user has elevated privileges upon this object
    // the current user must have the privilege to invite students to this study program
    // in order to overcome the validation of the active enrollment period
    const {target, model} = event;
    const state = PermissionMask.Execute;
    const privilege = 'StudyProgramRegisterAction/IgnoreEnrollmentPeriod';
    const throwError = false;
    const ignoreEnrollmentPeriod = await new Promise((resolve, reject) => {
        const validateEvent = {
            target,
            model,
            state,
            privilege,
            throwError
        };
        new DataPermissionEventListener().validate(validateEvent, (err) => {
            if (err) {
                return reject(err);
            }
            return resolve(validateEvent.result === true);
        });
    });
    if (ignoreEnrollmentPeriod === true) {
        return;
    }
    if (owner === context.user.name) {
        // validate period
        if (enrollmentPeriod == null) {
            throw Object.assign(new DataError('ERR_MISSING_PERIOD', 'The enrollment period of the specified study program cannot be found', null, 'StudyProgramEnrollmentEvent'), {
                statusCode: 409
            });
        }
        const valid = validateEnrollmentPeriod(enrollmentPeriod);
        if (valid === false) {
            throw Object.assign(new DataError('ERR_EXPIRED_PERIOD', 'The enrollment period has been expired', null, 'StudyProgramEnrollmentEvent'), {
                statusCode: 409
            });
        }
    }
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return beforeSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}