import { DataObjectState } from "@themost/data";
/**
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {
    const context = event.model.context;
    // get current status
    const actionStatus = await context.model('StudentPersonUpdateAction')
        .where('id').equal(event.target.id).select('actionStatus/alternateName').value();

    let shouldUpdate = false;
    if (event.state === DataObjectState.Update) {
        if (actionStatus !== 'CompletedActionStatus') {
            return;
        }
        // get previous status
        let previousStatus = 'UnknownActionStatus';
        if (event.previous) {
            previousStatus = event.previous.actionStatus.alternateName;
        } else {
            throw new DataError('E_STATE', 'The previous state of an object cannot be determined', null, 'StudyProgramRegisterAction');
        }
        if (previousStatus === 'ActiveActionStatus') {
            // update student record
            shouldUpdate = true;
        }
    }
    if (shouldUpdate) {
        const item = await context.model('StudentPersonUpdateAction').where('id').equal(event.target.id).getItem();
        // update student and person
        const updateStudent = {
            id: item.student,
            studentInstituteIdentifier: item.studentInstituteIdentifier,
            homeInstitute: item.homeInstitute,
            homeDepartment: item.homeDepartment,
            homeDepartmentDescription: item.homeDepartmentDescription,
            person: {
                fatherName: item.fatherName,
                motherName: item.motherName,
                birthDate: item.birthDate,
                birthPlace: item.birthPlace,
                birthCountry: item.birthCountry
            }
        }
        await context.model('Student').save(updateStudent);
    }

}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}