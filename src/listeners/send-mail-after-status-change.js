import { getMailer } from '@themost/mailer';
import { DataError } from '@themost/data';
/**
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {
    if (event.state === 2) {
        const context = event.model.context;
        /**
         * @type {import('@themost/mailer').MailerHelper}
         */
        const mailer = getMailer(context);

        const actionStatus = await context.model('StudyProgramRegisterAction')
            .where('id').equal(event.target.id).select('actionStatus/alternateName').value();
        let previousStatus = 'UnknownActionStatus';
        if (event.previous) {
            previousStatus = event.previous.actionStatus.alternateName;
        } else {
            throw new DataError('E_STATE', 'The previous state of an object cannot be determined', null, 'StudyProgramRegisterAction');
        }
        if (actionStatus === previousStatus) {
            return;
        }
        // get parent action data
        const item = await context.model('StudyProgramRegisterAction')
            .where('id').equal(event.target.id)
            .expand('candidate', 'owner')
            .silent().getItem();
        if (item) {
            await new Promise((resolve, reject) => {
                mailer.template('register-status-change')
                    .subject('EPICUR - Application status has been changed')
                    .to(item.owner.name)
                    .send({
                        model: item
                    }, (err) => {
                        if (err) {
                            return reject(err);
                        }
                        return resolve();
                    })
            });

        }
    }
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}