import { DataError } from '@themost/data';
/**
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {
    const actionStatus = await event.model.where('id').equal(event.target.id).select('actionStatus/alternateName').value();
    if (actionStatus === 'ActiveActionStatus') {
        const context = event.model.context;
        await new Promise((resolve, reject) => {
            // complete action
            context.unattended((cb) => {
                return context.model(event.model.name).save({
                    id: event.target.id,
                    actionStatus: {
                        alternateName: 'CompletedActionStatus'
                    }
                }).then(() => {
                    return cb();
                }).catch((err) => {
                    return cb(err);
                });
            }, (err) => {
                if (err) {
                    reject(err);
                }
                return resolve();
            });
        });
    }
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}