import { getMailer } from '@themost/mailer';
import { DataError, DataObjectState } from '@themost/data';
import moment from 'moment';
import { TraceUtils } from '@themost/common';
/**
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {
    if (event.state === DataObjectState.Insert) {
        const context = event.model.context;
        /**
         * @type {import('@themost/mailer').MailerHelper}
         */
        const mailer = getMailer(context);

        const exists = await context.model('CourseClassRegisterAction')
            .where('initiator').equal(event.target.id)
            .and('additionalType').equal('CourseClassInviteAction')
            .count();
        if (exists > 0) {
            const mailConfiguration = await context.model('MailConfiguration').where(
                'target'
            ).equal('CourseClassInviteAction')
            .and('state').equal(DataObjectState.Insert)
            .and('inLanguage').equal('en').silent().getItem();
            if (mailConfiguration == null) {
                return;
            }
            const recipient = await context.model(event.model.name).where('id').equal(event.target.id)
                .select('candidate/email').value();
            if (recipient == null) {
                TraceUtils.warn(`The recipient of the application with id ${event.target.id} cannot be found.`);
                return;
            }
            const { subject, body } = mailConfiguration;
            await new Promise((resolve, reject) => {
                mailer.body(body)
                    .subject(subject)
                    .to(recipient)
                    .send({
                        html: {
                            moment
                        }
                    }, (err) => {
                        if (err) {
                            TraceUtils.error(`An error occurred while sending mail for course invitation to the recipient of the application with id ${event.target.id}.`);
                            TraceUtils.error(err);
                            return resolve();
                        }
                        return resolve();
                    });
            });
        }
    }
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}