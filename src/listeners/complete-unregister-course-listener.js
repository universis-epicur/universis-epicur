import { DataError } from '@themost/common';
import { DataObject } from '@themost/data';
/**
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {
    const actionStatus = await event.model.where('id').equal(event.target.id).select('actionStatus/alternateName').value();
    if (actionStatus === 'CompletedActionStatus') {
        // get item
        const context = event.model.context;
        const item = await event.model.where('id').equal(event.target.id).getItem();
        // find student registration
        const studentRegistration = context.model('StudentPeriodRegistration')
            .where('student').equal(item.student)
            .and('classes/id').equal(item.courseClass)
            .expand('classes')
            .getΤypedItem();
        if (studentRegistration == null) {
            throw new DataError('ERR_MISSING_REGISTRATION', 'Student period registration cannot be found or is inaccessible', null, 'StudentPeriodRegistration');
        }
        /**
         * @type {DataObject}
         */
        const courseClass = context.model('CourseClass').convert(item.courseClass);
        const findIndex = studentRegistration.classes.findIndex((element) => {
            return element.id === courseClass.getId();
        });
        if (findIndex < 0) {
            throw new DataError('ERR_MISSING_COURSE', 'Student course registration cannot be found or is inaccessible', null, 'StudentCourseClass');
        }
        // remove course class
        studentRegistration.classes.splice(findIndex, 1);
        // save student registration
        await studentRegistration.saveOne(context);
    }
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}