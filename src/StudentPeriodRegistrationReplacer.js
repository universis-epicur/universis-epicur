import { ApplicationService, DataError, TraceUtils } from "@themost/common";
import { ModelClassLoaderStrategy, SchemaLoaderStrategy } from "@themost/data";
import { EdmMapping, DataObject } from '@themost/data';

class StudentPeriodRegistration extends DataObject {
    @EdmMapping.action('sync', 'Object')
    async sync() {
        TraceUtils.log(`(StudentPeriodRegistration.sync) Syncing student period registration ID:${this.getId()}`);
        const student = await this.property('student').expand('user', 'person', 'homeInstitute').silent().getItem();
        if (student == null) {
            throw new DataError('E_STUDENT', 'Student cannot be found or is inaccessible', null, 'StudentPeriodRegistration', 'student');
        }
        TraceUtils.log(`(StudentPeriodRegistration.sync) Student period registration student:${student.id}`);
        // get courses
        const courseClasses = await this.property('classes').expand('course').silent().getItems();
        /**
         * @type {EpicurService}
         */
        const service = this.context.getApplication().getService(function EpicurService() { });
        // get token
        const token = await service.getServiceToken();
        // try to get user
        let user = await service.getUserByName(student.user.name, {
            token: token
        });
        if (user == null) {
            TraceUtils.log(`(StudentPeriodRegistration.sync) VCLP user cannot be found and it's going to be added user:${student.user.name}`);
            class User { }
            const newUser = Object.assign(new User, {
                Login: student.user.name,
                ExternalAccount: student.user.name,
                Role: null,
                Firstname: student.person.givenName,
                Lastname: student.person.familyName,
                Email: student.person.email,
                Institution: student.homeInstitute && student.homeInstitute.name,
                Active: true,
                AuthMode: null
            });
            // add user
            await service.addUser(newUser, {
                token: token
            });
            // get user again
            user = await service.getUserByName(student.user.name, {
                token: token
            });
            if (user == null) {
                throw new DataError('E_VCLP_USER', 'VCLP user cannot be found or is inaccessible', null, 'Student', 'user');
            }
        }
        TraceUtils.log(`(StudentPeriodRegistration.sync) Student period registration ${this.getId()}: Getting user from VCLP`);
        // sync user
        // eslint-disable-next-line no-unused-vars
        let sync = 0;
        // eslint-disable-next-line no-unused-vars
        for (const courseClass of courseClasses) {
            TraceUtils.log(`(StudentPeriodRegistration.sync) Student period registration ${this.getId()}: Updating course class ${courseClass.id}`);
            if (courseClass.course.identifier) {
                TraceUtils.log(`(StudentPeriodRegistration.sync) Student period registration ${this.getId()}: Get course roles from VCLP`);
                // try to add course roles
                let findRole = null;
                try {
                    let courseRoles = await service.getCourseRoles(courseClass.course.identifier, {
                        token: token
                    });
                    TraceUtils.log(`(StudentPeriodRegistration.sync) Student period registration ${this.getId()}: Find course role from VCLP`);
                    findRole = courseRoles.find((item) => {
                        return item.Title === 'VURS Students';
                    });
                } catch (err) {
                    TraceUtils.error(`(StudentPeriodRegistration.sync) An error occurred while querying course roles for VCLP course with id ${courseClass.course.identifier}.`);
                    TraceUtils.error(err);
                }
                if (findRole == null) {
                    // add role
                    findRole = await service.addCourseRole(courseClass.course.identifier, {
                        "Title": "VURS Students",
                        "Description": "Student role created by the VURS"
                    }, {
                        token: token
                    });
                }
                TraceUtils.log(`(StudentPeriodRegistration.sync) Student course registration ${courseClass.id}: Assign role to user`);
                // assign user to course role
                await service.assignUserRole(findRole.ID, user.ID, {
                    token: token
                });
                sync += 1;
            } else {
                TraceUtils.log(`(StudentPeriodRegistration.sync) Student period registration ${this.getId()}: VCLP Course identifier is missing ${courseClass.course.id} ${courseClass.course.name}`);
            }
        }
        let syncStatus = 'unknown';
        if (sync === courseClasses.length) {
            syncStatus = 'completed';
        } else {
            syncStatus = 'partial';
        }
        // update item
        const item = await this.context.model('StudentPeriodRegistration')
            .where('id').equal(this.getId())
            .silent()
            .getItem();
        Object.assign(item, {
            syncStatus: syncStatus
        });
        await this.context.model('StudentPeriodRegistration').silent().save(item);
    }
}

export class StudentPeriodRegistrationReplacer extends ApplicationService {
    constructor(app) {
        super(app);
    }

    apply() {
        // get schema loader
        const schemaLoader = this.getApplication().getConfiguration().getStrategy(SchemaLoaderStrategy);
        // get model definition
        const model = schemaLoader.getModelDefinition('StudentPeriodRegistration');
        // get model class
        const loader = this.getApplication().getConfiguration().getStrategy(ModelClassLoaderStrategy);
        const StudentPeriodRegistrationClass = loader.resolve(model);
        // override createDocument() method to nothing
        Object.assign(StudentPeriodRegistrationClass.prototype, {
            createDocument(callback) {
                return callback();
            }
        });
        StudentPeriodRegistrationClass.prototype.sync = StudentPeriodRegistration.prototype.sync;
    }

}

